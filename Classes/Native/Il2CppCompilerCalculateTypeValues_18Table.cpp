﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// CRGTFader
struct CRGTFader_t912498268;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t701940803;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t1884415901;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t768590915;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t3913627115;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t2311174851;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t3841783507;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t235587086;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t240936516;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2748928575;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t2508470592;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.GUIText
struct GUIText_t402233326;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// CRGTSpriteToggle[]
struct CRGTSpriteToggleU5BU5D_t2845023549;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef U3CSTARTFADEU3EC__ITERATOR0_T3292164234_H
#define U3CSTARTFADEU3EC__ITERATOR0_T3292164234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTFader/<StartFade>c__Iterator0
struct  U3CStartFadeU3Ec__Iterator0_t3292164234  : public RuntimeObject
{
public:
	// System.Single CRGTFader/<StartFade>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// System.Single CRGTFader/<StartFade>c__Iterator0::<wait>__0
	float ___U3CwaitU3E__0_1;
	// System.Boolean CRGTFader/<StartFade>c__Iterator0::fade
	bool ___fade_2;
	// CRGTFader CRGTFader/<StartFade>c__Iterator0::$this
	CRGTFader_t912498268 * ___U24this_3;
	// System.Object CRGTFader/<StartFade>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean CRGTFader/<StartFade>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 CRGTFader/<StartFade>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwaitU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U3CwaitU3E__0_1)); }
	inline float get_U3CwaitU3E__0_1() const { return ___U3CwaitU3E__0_1; }
	inline float* get_address_of_U3CwaitU3E__0_1() { return &___U3CwaitU3E__0_1; }
	inline void set_U3CwaitU3E__0_1(float value)
	{
		___U3CwaitU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_fade_2() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___fade_2)); }
	inline bool get_fade_2() const { return ___fade_2; }
	inline bool* get_address_of_fade_2() { return &___fade_2; }
	inline void set_fade_2(bool value)
	{
		___fade_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24this_3)); }
	inline CRGTFader_t912498268 * get_U24this_3() const { return ___U24this_3; }
	inline CRGTFader_t912498268 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(CRGTFader_t912498268 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFADEU3EC__ITERATOR0_T3292164234_H
#ifndef REFLECTIONMETHODSCACHE_T2103211062_H
#define REFLECTIONMETHODSCACHE_T2103211062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t2103211062  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t701940803 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t1884415901 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t768590915 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t3913627115 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t2311174851 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t3841783507 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3D_0)); }
	inline Raycast3DCallback_t701940803 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t701940803 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t701940803 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t1884415901 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t1884415901 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t1884415901 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast2D_2)); }
	inline Raycast2DCallback_t768590915 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t768590915 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t768590915 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t3913627115 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t3913627115 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t3913627115 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t2311174851 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t3841783507 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t3841783507 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t3841783507 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t2103211062_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t2103211062 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t2103211062 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t2103211062 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t2103211062 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T2103211062_H
#ifndef LAYOUTUTILITY_T2745813735_H
#define LAYOUTUTILITY_T2745813735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t2745813735  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t2745813735_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t235587086 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t235587086 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t235587086 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t235587086 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t235587086 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t235587086 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t235587086 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t235587086 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T2745813735_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t3704657025 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___rectTransform_0)); }
	inline RectTransform_t3704657025 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3704657025 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifndef LAYOUTREBUILDER_T541313304_H
#define LAYOUTREBUILDER_T541313304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t541313304  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3704657025 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_ToRebuild_0)); }
	inline RectTransform_t3704657025 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3704657025 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3704657025 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t541313304_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t240936516 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t1258266594 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2748928575 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t240936516 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t240936516 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t240936516 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t1258266594 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t1258266594 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2748928575 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2748928575 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2748928575 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T541313304_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-AC9373DEC6917FF369D56F004DADC890EAA43D80
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0;

public:
	inline static int32_t get_offset_of_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0() const { return ___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0() { return &___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0; }
	inline void set_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef GAMEMODE_T1892292441_H
#define GAMEMODE_T1892292441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMode
struct  GameMode_t1892292441 
{
public:
	// System.Int32 GameMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameMode_t1892292441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMODE_T1892292441_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef CONTROLTYPE_T166092394_H
#define CONTROLTYPE_T166092394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlType
struct  ControlType_t166092394 
{
public:
	// System.Int32 ControlType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlType_t166092394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTYPE_T166092394_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GB_T166534947_H
#define GB_T166534947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GB
struct  GB_t166534947  : public RuntimeObject
{
public:

public:
};

struct GB_t166534947_StaticFields
{
public:
	// GameMode GB::m_GameMode
	int32_t ___m_GameMode_0;
	// System.Int32 GB::g_GameScore
	int32_t ___g_GameScore_1;
	// System.String GB::g_LangType
	String_t* ___g_LangType_2;

public:
	inline static int32_t get_offset_of_m_GameMode_0() { return static_cast<int32_t>(offsetof(GB_t166534947_StaticFields, ___m_GameMode_0)); }
	inline int32_t get_m_GameMode_0() const { return ___m_GameMode_0; }
	inline int32_t* get_address_of_m_GameMode_0() { return &___m_GameMode_0; }
	inline void set_m_GameMode_0(int32_t value)
	{
		___m_GameMode_0 = value;
	}

	inline static int32_t get_offset_of_g_GameScore_1() { return static_cast<int32_t>(offsetof(GB_t166534947_StaticFields, ___g_GameScore_1)); }
	inline int32_t get_g_GameScore_1() const { return ___g_GameScore_1; }
	inline int32_t* get_address_of_g_GameScore_1() { return &___g_GameScore_1; }
	inline void set_g_GameScore_1(int32_t value)
	{
		___g_GameScore_1 = value;
	}

	inline static int32_t get_offset_of_g_LangType_2() { return static_cast<int32_t>(offsetof(GB_t166534947_StaticFields, ___g_LangType_2)); }
	inline String_t* get_g_LangType_2() const { return ___g_LangType_2; }
	inline String_t** get_address_of_g_LangType_2() { return &___g_LangType_2; }
	inline void set_g_LangType_2(String_t* value)
	{
		___g_LangType_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_LangType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GB_T166534947_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CRGTMATERIALMOVER_T3899902404_H
#define CRGTMATERIALMOVER_T3899902404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTMaterialMover
struct  CRGTMaterialMover_t3899902404  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTMaterialMover::materialSpeedX
	float ___materialSpeedX_2;
	// System.Single CRGTMaterialMover::materialSpeedY
	float ___materialSpeedY_3;
	// UnityEngine.Vector2 CRGTMaterialMover::materialOffset
	Vector2_t2156229523  ___materialOffset_4;
	// UnityEngine.Renderer CRGTMaterialMover::materialRenderer
	Renderer_t2627027031 * ___materialRenderer_5;

public:
	inline static int32_t get_offset_of_materialSpeedX_2() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialSpeedX_2)); }
	inline float get_materialSpeedX_2() const { return ___materialSpeedX_2; }
	inline float* get_address_of_materialSpeedX_2() { return &___materialSpeedX_2; }
	inline void set_materialSpeedX_2(float value)
	{
		___materialSpeedX_2 = value;
	}

	inline static int32_t get_offset_of_materialSpeedY_3() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialSpeedY_3)); }
	inline float get_materialSpeedY_3() const { return ___materialSpeedY_3; }
	inline float* get_address_of_materialSpeedY_3() { return &___materialSpeedY_3; }
	inline void set_materialSpeedY_3(float value)
	{
		___materialSpeedY_3 = value;
	}

	inline static int32_t get_offset_of_materialOffset_4() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialOffset_4)); }
	inline Vector2_t2156229523  get_materialOffset_4() const { return ___materialOffset_4; }
	inline Vector2_t2156229523 * get_address_of_materialOffset_4() { return &___materialOffset_4; }
	inline void set_materialOffset_4(Vector2_t2156229523  value)
	{
		___materialOffset_4 = value;
	}

	inline static int32_t get_offset_of_materialRenderer_5() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialRenderer_5)); }
	inline Renderer_t2627027031 * get_materialRenderer_5() const { return ___materialRenderer_5; }
	inline Renderer_t2627027031 ** get_address_of_materialRenderer_5() { return &___materialRenderer_5; }
	inline void set_materialRenderer_5(Renderer_t2627027031 * value)
	{
		___materialRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___materialRenderer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTMATERIALMOVER_T3899902404_H
#ifndef CRGTOBJECTDESTROYER_T475906085_H
#define CRGTOBJECTDESTROYER_T475906085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTObjectDestroyer
struct  CRGTObjectDestroyer_t475906085  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CRGTObjectDestroyer::removeTarget
	bool ___removeTarget_2;
	// System.Single CRGTObjectDestroyer::removeTargetTime
	float ___removeTargetTime_3;
	// System.Boolean CRGTObjectDestroyer::removeSelf
	bool ___removeSelf_4;
	// System.Single CRGTObjectDestroyer::removeSelfTime
	float ___removeSelfTime_5;

public:
	inline static int32_t get_offset_of_removeTarget_2() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeTarget_2)); }
	inline bool get_removeTarget_2() const { return ___removeTarget_2; }
	inline bool* get_address_of_removeTarget_2() { return &___removeTarget_2; }
	inline void set_removeTarget_2(bool value)
	{
		___removeTarget_2 = value;
	}

	inline static int32_t get_offset_of_removeTargetTime_3() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeTargetTime_3)); }
	inline float get_removeTargetTime_3() const { return ___removeTargetTime_3; }
	inline float* get_address_of_removeTargetTime_3() { return &___removeTargetTime_3; }
	inline void set_removeTargetTime_3(float value)
	{
		___removeTargetTime_3 = value;
	}

	inline static int32_t get_offset_of_removeSelf_4() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeSelf_4)); }
	inline bool get_removeSelf_4() const { return ___removeSelf_4; }
	inline bool* get_address_of_removeSelf_4() { return &___removeSelf_4; }
	inline void set_removeSelf_4(bool value)
	{
		___removeSelf_4 = value;
	}

	inline static int32_t get_offset_of_removeSelfTime_5() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeSelfTime_5)); }
	inline float get_removeSelfTime_5() const { return ___removeSelfTime_5; }
	inline float* get_address_of_removeSelfTime_5() { return &___removeSelfTime_5; }
	inline void set_removeSelfTime_5(float value)
	{
		___removeSelfTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTOBJECTDESTROYER_T475906085_H
#ifndef CRGTOBJECTMOVER_T1733178804_H
#define CRGTOBJECTMOVER_T1733178804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTObjectMover
struct  CRGTObjectMover_t1733178804  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTObjectMover::objectSpeed
	float ___objectSpeed_2;
	// System.Single CRGTObjectMover::objectMoveX
	float ___objectMoveX_3;
	// System.Single CRGTObjectMover::objectMoveY
	float ___objectMoveY_4;
	// System.Single CRGTObjectMover::objectMoveZ
	float ___objectMoveZ_5;

public:
	inline static int32_t get_offset_of_objectSpeed_2() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectSpeed_2)); }
	inline float get_objectSpeed_2() const { return ___objectSpeed_2; }
	inline float* get_address_of_objectSpeed_2() { return &___objectSpeed_2; }
	inline void set_objectSpeed_2(float value)
	{
		___objectSpeed_2 = value;
	}

	inline static int32_t get_offset_of_objectMoveX_3() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectMoveX_3)); }
	inline float get_objectMoveX_3() const { return ___objectMoveX_3; }
	inline float* get_address_of_objectMoveX_3() { return &___objectMoveX_3; }
	inline void set_objectMoveX_3(float value)
	{
		___objectMoveX_3 = value;
	}

	inline static int32_t get_offset_of_objectMoveY_4() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectMoveY_4)); }
	inline float get_objectMoveY_4() const { return ___objectMoveY_4; }
	inline float* get_address_of_objectMoveY_4() { return &___objectMoveY_4; }
	inline void set_objectMoveY_4(float value)
	{
		___objectMoveY_4 = value;
	}

	inline static int32_t get_offset_of_objectMoveZ_5() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectMoveZ_5)); }
	inline float get_objectMoveZ_5() const { return ___objectMoveZ_5; }
	inline float* get_address_of_objectMoveZ_5() { return &___objectMoveZ_5; }
	inline void set_objectMoveZ_5(float value)
	{
		___objectMoveZ_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTOBJECTMOVER_T1733178804_H
#ifndef CRGTGAMEMANAGER_T3284010685_H
#define CRGTGAMEMANAGER_T3284010685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTGameManager
struct  CRGTGameManager_t3284010685  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTGameManager::startGameSpeed
	float ___startGameSpeed_3;
	// System.Single CRGTGameManager::gameSpeed
	float ___gameSpeed_4;
	// System.Int32 CRGTGameManager::highGameScore
	int32_t ___highGameScore_5;
	// System.Int32 CRGTGameManager::lastHighGameScore
	int32_t ___lastHighGameScore_6;
	// System.Int32 CRGTGameManager::lastGameScore
	int32_t ___lastGameScore_7;
	// System.Int32 CRGTGameManager::bonusGameCount
	int32_t ___bonusGameCount_8;
	// System.Single CRGTGameManager::spawnTime
	float ___spawnTime_9;
	// System.Single CRGTGameManager::spawnSpeed
	float ___spawnSpeed_10;
	// System.Boolean CRGTGameManager::isGameOver
	bool ___isGameOver_11;
	// System.Boolean CRGTGameManager::isGamePaused
	bool ___isGamePaused_12;
	// System.Int32 CRGTGameManager::gameScore
	int32_t ___gameScore_13;
	// System.Single CRGTGameManager::startSpawnSpeed
	float ___startSpawnSpeed_14;
	// System.Single CRGTGameManager::spawnStep
	float ___spawnStep_15;
	// System.Single CRGTGameManager::minSpawSpeed
	float ___minSpawSpeed_16;
	// UnityEngine.RectTransform CRGTGameManager::spawnLine
	RectTransform_t3704657025 * ___spawnLine_17;
	// System.Single[] CRGTGameManager::spawnObjectsXPos
	SingleU5BU5D_t1444911251* ___spawnObjectsXPos_18;
	// UnityEngine.GameObject[] CRGTGameManager::spawnGameObjects
	GameObjectU5BU5D_t3328599146* ___spawnGameObjects_19;
	// UnityEngine.GameObject CRGTGameManager::spawnObject
	GameObject_t1113636619 * ___spawnObject_20;
	// UnityEngine.AudioClip CRGTGameManager::buttonClick
	AudioClip_t3680889665 * ___buttonClick_21;
	// UnityEngine.UI.Text CRGTGameManager::gameScoreText
	Text_t1901882714 * ___gameScoreText_22;
	// UnityEngine.UI.Text CRGTGameManager::gameBestScoreText
	Text_t1901882714 * ___gameBestScoreText_23;
	// UnityEngine.UI.Text CRGTGameManager::gameLastScoreText
	Text_t1901882714 * ___gameLastScoreText_24;
	// UnityEngine.UI.Text CRGTGameManager::bonusGameCountText
	Text_t1901882714 * ___bonusGameCountText_25;
	// UnityEngine.UI.Text CRGTGameManager::gameOverScoreText
	Text_t1901882714 * ___gameOverScoreText_26;
	// UnityEngine.UI.Text CRGTGameManager::gameOverNewText
	Text_t1901882714 * ___gameOverNewText_27;
	// UnityEngine.UI.Text CRGTGameManager::gameOverHighScoreText
	Text_t1901882714 * ___gameOverHighScoreText_28;
	// UnityEngine.UI.Text CRGTGameManager::gameOverBonusCountText
	Text_t1901882714 * ___gameOverBonusCountText_29;
	// UnityEngine.GameObject CRGTGameManager::menuCanvas
	GameObject_t1113636619 * ___menuCanvas_30;
	// UnityEngine.GameObject CRGTGameManager::gameCanvas
	GameObject_t1113636619 * ___gameCanvas_31;
	// UnityEngine.GameObject CRGTGameManager::pauseCanvas
	GameObject_t1113636619 * ___pauseCanvas_32;
	// UnityEngine.GameObject CRGTGameManager::gameOverCanvas
	GameObject_t1113636619 * ___gameOverCanvas_33;
	// System.String CRGTGameManager::gameOverURL
	String_t* ___gameOverURL_34;

public:
	inline static int32_t get_offset_of_startGameSpeed_3() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___startGameSpeed_3)); }
	inline float get_startGameSpeed_3() const { return ___startGameSpeed_3; }
	inline float* get_address_of_startGameSpeed_3() { return &___startGameSpeed_3; }
	inline void set_startGameSpeed_3(float value)
	{
		___startGameSpeed_3 = value;
	}

	inline static int32_t get_offset_of_gameSpeed_4() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameSpeed_4)); }
	inline float get_gameSpeed_4() const { return ___gameSpeed_4; }
	inline float* get_address_of_gameSpeed_4() { return &___gameSpeed_4; }
	inline void set_gameSpeed_4(float value)
	{
		___gameSpeed_4 = value;
	}

	inline static int32_t get_offset_of_highGameScore_5() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___highGameScore_5)); }
	inline int32_t get_highGameScore_5() const { return ___highGameScore_5; }
	inline int32_t* get_address_of_highGameScore_5() { return &___highGameScore_5; }
	inline void set_highGameScore_5(int32_t value)
	{
		___highGameScore_5 = value;
	}

	inline static int32_t get_offset_of_lastHighGameScore_6() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___lastHighGameScore_6)); }
	inline int32_t get_lastHighGameScore_6() const { return ___lastHighGameScore_6; }
	inline int32_t* get_address_of_lastHighGameScore_6() { return &___lastHighGameScore_6; }
	inline void set_lastHighGameScore_6(int32_t value)
	{
		___lastHighGameScore_6 = value;
	}

	inline static int32_t get_offset_of_lastGameScore_7() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___lastGameScore_7)); }
	inline int32_t get_lastGameScore_7() const { return ___lastGameScore_7; }
	inline int32_t* get_address_of_lastGameScore_7() { return &___lastGameScore_7; }
	inline void set_lastGameScore_7(int32_t value)
	{
		___lastGameScore_7 = value;
	}

	inline static int32_t get_offset_of_bonusGameCount_8() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___bonusGameCount_8)); }
	inline int32_t get_bonusGameCount_8() const { return ___bonusGameCount_8; }
	inline int32_t* get_address_of_bonusGameCount_8() { return &___bonusGameCount_8; }
	inline void set_bonusGameCount_8(int32_t value)
	{
		___bonusGameCount_8 = value;
	}

	inline static int32_t get_offset_of_spawnTime_9() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnTime_9)); }
	inline float get_spawnTime_9() const { return ___spawnTime_9; }
	inline float* get_address_of_spawnTime_9() { return &___spawnTime_9; }
	inline void set_spawnTime_9(float value)
	{
		___spawnTime_9 = value;
	}

	inline static int32_t get_offset_of_spawnSpeed_10() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnSpeed_10)); }
	inline float get_spawnSpeed_10() const { return ___spawnSpeed_10; }
	inline float* get_address_of_spawnSpeed_10() { return &___spawnSpeed_10; }
	inline void set_spawnSpeed_10(float value)
	{
		___spawnSpeed_10 = value;
	}

	inline static int32_t get_offset_of_isGameOver_11() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___isGameOver_11)); }
	inline bool get_isGameOver_11() const { return ___isGameOver_11; }
	inline bool* get_address_of_isGameOver_11() { return &___isGameOver_11; }
	inline void set_isGameOver_11(bool value)
	{
		___isGameOver_11 = value;
	}

	inline static int32_t get_offset_of_isGamePaused_12() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___isGamePaused_12)); }
	inline bool get_isGamePaused_12() const { return ___isGamePaused_12; }
	inline bool* get_address_of_isGamePaused_12() { return &___isGamePaused_12; }
	inline void set_isGamePaused_12(bool value)
	{
		___isGamePaused_12 = value;
	}

	inline static int32_t get_offset_of_gameScore_13() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameScore_13)); }
	inline int32_t get_gameScore_13() const { return ___gameScore_13; }
	inline int32_t* get_address_of_gameScore_13() { return &___gameScore_13; }
	inline void set_gameScore_13(int32_t value)
	{
		___gameScore_13 = value;
	}

	inline static int32_t get_offset_of_startSpawnSpeed_14() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___startSpawnSpeed_14)); }
	inline float get_startSpawnSpeed_14() const { return ___startSpawnSpeed_14; }
	inline float* get_address_of_startSpawnSpeed_14() { return &___startSpawnSpeed_14; }
	inline void set_startSpawnSpeed_14(float value)
	{
		___startSpawnSpeed_14 = value;
	}

	inline static int32_t get_offset_of_spawnStep_15() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnStep_15)); }
	inline float get_spawnStep_15() const { return ___spawnStep_15; }
	inline float* get_address_of_spawnStep_15() { return &___spawnStep_15; }
	inline void set_spawnStep_15(float value)
	{
		___spawnStep_15 = value;
	}

	inline static int32_t get_offset_of_minSpawSpeed_16() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___minSpawSpeed_16)); }
	inline float get_minSpawSpeed_16() const { return ___minSpawSpeed_16; }
	inline float* get_address_of_minSpawSpeed_16() { return &___minSpawSpeed_16; }
	inline void set_minSpawSpeed_16(float value)
	{
		___minSpawSpeed_16 = value;
	}

	inline static int32_t get_offset_of_spawnLine_17() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnLine_17)); }
	inline RectTransform_t3704657025 * get_spawnLine_17() const { return ___spawnLine_17; }
	inline RectTransform_t3704657025 ** get_address_of_spawnLine_17() { return &___spawnLine_17; }
	inline void set_spawnLine_17(RectTransform_t3704657025 * value)
	{
		___spawnLine_17 = value;
		Il2CppCodeGenWriteBarrier((&___spawnLine_17), value);
	}

	inline static int32_t get_offset_of_spawnObjectsXPos_18() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnObjectsXPos_18)); }
	inline SingleU5BU5D_t1444911251* get_spawnObjectsXPos_18() const { return ___spawnObjectsXPos_18; }
	inline SingleU5BU5D_t1444911251** get_address_of_spawnObjectsXPos_18() { return &___spawnObjectsXPos_18; }
	inline void set_spawnObjectsXPos_18(SingleU5BU5D_t1444911251* value)
	{
		___spawnObjectsXPos_18 = value;
		Il2CppCodeGenWriteBarrier((&___spawnObjectsXPos_18), value);
	}

	inline static int32_t get_offset_of_spawnGameObjects_19() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnGameObjects_19)); }
	inline GameObjectU5BU5D_t3328599146* get_spawnGameObjects_19() const { return ___spawnGameObjects_19; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_spawnGameObjects_19() { return &___spawnGameObjects_19; }
	inline void set_spawnGameObjects_19(GameObjectU5BU5D_t3328599146* value)
	{
		___spawnGameObjects_19 = value;
		Il2CppCodeGenWriteBarrier((&___spawnGameObjects_19), value);
	}

	inline static int32_t get_offset_of_spawnObject_20() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnObject_20)); }
	inline GameObject_t1113636619 * get_spawnObject_20() const { return ___spawnObject_20; }
	inline GameObject_t1113636619 ** get_address_of_spawnObject_20() { return &___spawnObject_20; }
	inline void set_spawnObject_20(GameObject_t1113636619 * value)
	{
		___spawnObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___spawnObject_20), value);
	}

	inline static int32_t get_offset_of_buttonClick_21() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___buttonClick_21)); }
	inline AudioClip_t3680889665 * get_buttonClick_21() const { return ___buttonClick_21; }
	inline AudioClip_t3680889665 ** get_address_of_buttonClick_21() { return &___buttonClick_21; }
	inline void set_buttonClick_21(AudioClip_t3680889665 * value)
	{
		___buttonClick_21 = value;
		Il2CppCodeGenWriteBarrier((&___buttonClick_21), value);
	}

	inline static int32_t get_offset_of_gameScoreText_22() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameScoreText_22)); }
	inline Text_t1901882714 * get_gameScoreText_22() const { return ___gameScoreText_22; }
	inline Text_t1901882714 ** get_address_of_gameScoreText_22() { return &___gameScoreText_22; }
	inline void set_gameScoreText_22(Text_t1901882714 * value)
	{
		___gameScoreText_22 = value;
		Il2CppCodeGenWriteBarrier((&___gameScoreText_22), value);
	}

	inline static int32_t get_offset_of_gameBestScoreText_23() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameBestScoreText_23)); }
	inline Text_t1901882714 * get_gameBestScoreText_23() const { return ___gameBestScoreText_23; }
	inline Text_t1901882714 ** get_address_of_gameBestScoreText_23() { return &___gameBestScoreText_23; }
	inline void set_gameBestScoreText_23(Text_t1901882714 * value)
	{
		___gameBestScoreText_23 = value;
		Il2CppCodeGenWriteBarrier((&___gameBestScoreText_23), value);
	}

	inline static int32_t get_offset_of_gameLastScoreText_24() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameLastScoreText_24)); }
	inline Text_t1901882714 * get_gameLastScoreText_24() const { return ___gameLastScoreText_24; }
	inline Text_t1901882714 ** get_address_of_gameLastScoreText_24() { return &___gameLastScoreText_24; }
	inline void set_gameLastScoreText_24(Text_t1901882714 * value)
	{
		___gameLastScoreText_24 = value;
		Il2CppCodeGenWriteBarrier((&___gameLastScoreText_24), value);
	}

	inline static int32_t get_offset_of_bonusGameCountText_25() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___bonusGameCountText_25)); }
	inline Text_t1901882714 * get_bonusGameCountText_25() const { return ___bonusGameCountText_25; }
	inline Text_t1901882714 ** get_address_of_bonusGameCountText_25() { return &___bonusGameCountText_25; }
	inline void set_bonusGameCountText_25(Text_t1901882714 * value)
	{
		___bonusGameCountText_25 = value;
		Il2CppCodeGenWriteBarrier((&___bonusGameCountText_25), value);
	}

	inline static int32_t get_offset_of_gameOverScoreText_26() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverScoreText_26)); }
	inline Text_t1901882714 * get_gameOverScoreText_26() const { return ___gameOverScoreText_26; }
	inline Text_t1901882714 ** get_address_of_gameOverScoreText_26() { return &___gameOverScoreText_26; }
	inline void set_gameOverScoreText_26(Text_t1901882714 * value)
	{
		___gameOverScoreText_26 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScoreText_26), value);
	}

	inline static int32_t get_offset_of_gameOverNewText_27() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverNewText_27)); }
	inline Text_t1901882714 * get_gameOverNewText_27() const { return ___gameOverNewText_27; }
	inline Text_t1901882714 ** get_address_of_gameOverNewText_27() { return &___gameOverNewText_27; }
	inline void set_gameOverNewText_27(Text_t1901882714 * value)
	{
		___gameOverNewText_27 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverNewText_27), value);
	}

	inline static int32_t get_offset_of_gameOverHighScoreText_28() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverHighScoreText_28)); }
	inline Text_t1901882714 * get_gameOverHighScoreText_28() const { return ___gameOverHighScoreText_28; }
	inline Text_t1901882714 ** get_address_of_gameOverHighScoreText_28() { return &___gameOverHighScoreText_28; }
	inline void set_gameOverHighScoreText_28(Text_t1901882714 * value)
	{
		___gameOverHighScoreText_28 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverHighScoreText_28), value);
	}

	inline static int32_t get_offset_of_gameOverBonusCountText_29() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverBonusCountText_29)); }
	inline Text_t1901882714 * get_gameOverBonusCountText_29() const { return ___gameOverBonusCountText_29; }
	inline Text_t1901882714 ** get_address_of_gameOverBonusCountText_29() { return &___gameOverBonusCountText_29; }
	inline void set_gameOverBonusCountText_29(Text_t1901882714 * value)
	{
		___gameOverBonusCountText_29 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverBonusCountText_29), value);
	}

	inline static int32_t get_offset_of_menuCanvas_30() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___menuCanvas_30)); }
	inline GameObject_t1113636619 * get_menuCanvas_30() const { return ___menuCanvas_30; }
	inline GameObject_t1113636619 ** get_address_of_menuCanvas_30() { return &___menuCanvas_30; }
	inline void set_menuCanvas_30(GameObject_t1113636619 * value)
	{
		___menuCanvas_30 = value;
		Il2CppCodeGenWriteBarrier((&___menuCanvas_30), value);
	}

	inline static int32_t get_offset_of_gameCanvas_31() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameCanvas_31)); }
	inline GameObject_t1113636619 * get_gameCanvas_31() const { return ___gameCanvas_31; }
	inline GameObject_t1113636619 ** get_address_of_gameCanvas_31() { return &___gameCanvas_31; }
	inline void set_gameCanvas_31(GameObject_t1113636619 * value)
	{
		___gameCanvas_31 = value;
		Il2CppCodeGenWriteBarrier((&___gameCanvas_31), value);
	}

	inline static int32_t get_offset_of_pauseCanvas_32() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___pauseCanvas_32)); }
	inline GameObject_t1113636619 * get_pauseCanvas_32() const { return ___pauseCanvas_32; }
	inline GameObject_t1113636619 ** get_address_of_pauseCanvas_32() { return &___pauseCanvas_32; }
	inline void set_pauseCanvas_32(GameObject_t1113636619 * value)
	{
		___pauseCanvas_32 = value;
		Il2CppCodeGenWriteBarrier((&___pauseCanvas_32), value);
	}

	inline static int32_t get_offset_of_gameOverCanvas_33() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverCanvas_33)); }
	inline GameObject_t1113636619 * get_gameOverCanvas_33() const { return ___gameOverCanvas_33; }
	inline GameObject_t1113636619 ** get_address_of_gameOverCanvas_33() { return &___gameOverCanvas_33; }
	inline void set_gameOverCanvas_33(GameObject_t1113636619 * value)
	{
		___gameOverCanvas_33 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverCanvas_33), value);
	}

	inline static int32_t get_offset_of_gameOverURL_34() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverURL_34)); }
	inline String_t* get_gameOverURL_34() const { return ___gameOverURL_34; }
	inline String_t** get_address_of_gameOverURL_34() { return &___gameOverURL_34; }
	inline void set_gameOverURL_34(String_t* value)
	{
		___gameOverURL_34 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverURL_34), value);
	}
};

struct CRGTGameManager_t3284010685_StaticFields
{
public:
	// CRGTGameManager CRGTGameManager::instance
	CRGTGameManager_t3284010685 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685_StaticFields, ___instance_2)); }
	inline CRGTGameManager_t3284010685 * get_instance_2() const { return ___instance_2; }
	inline CRGTGameManager_t3284010685 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CRGTGameManager_t3284010685 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTGAMEMANAGER_T3284010685_H
#ifndef CRGTBONUS_T461169207_H
#define CRGTBONUS_T461169207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTBonus
struct  CRGTBonus_t461169207  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CRGTBonus::scoreEffect
	Transform_t3600365921 * ___scoreEffect_2;
	// UnityEngine.GameObject CRGTBonus::bonusParticles
	GameObject_t1113636619 * ___bonusParticles_3;
	// System.Int32 CRGTBonus::scoreValue
	int32_t ___scoreValue_4;
	// UnityEngine.AudioClip CRGTBonus::soundBonus
	AudioClip_t3680889665 * ___soundBonus_5;

public:
	inline static int32_t get_offset_of_scoreEffect_2() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___scoreEffect_2)); }
	inline Transform_t3600365921 * get_scoreEffect_2() const { return ___scoreEffect_2; }
	inline Transform_t3600365921 ** get_address_of_scoreEffect_2() { return &___scoreEffect_2; }
	inline void set_scoreEffect_2(Transform_t3600365921 * value)
	{
		___scoreEffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___scoreEffect_2), value);
	}

	inline static int32_t get_offset_of_bonusParticles_3() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___bonusParticles_3)); }
	inline GameObject_t1113636619 * get_bonusParticles_3() const { return ___bonusParticles_3; }
	inline GameObject_t1113636619 ** get_address_of_bonusParticles_3() { return &___bonusParticles_3; }
	inline void set_bonusParticles_3(GameObject_t1113636619 * value)
	{
		___bonusParticles_3 = value;
		Il2CppCodeGenWriteBarrier((&___bonusParticles_3), value);
	}

	inline static int32_t get_offset_of_scoreValue_4() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___scoreValue_4)); }
	inline int32_t get_scoreValue_4() const { return ___scoreValue_4; }
	inline int32_t* get_address_of_scoreValue_4() { return &___scoreValue_4; }
	inline void set_scoreValue_4(int32_t value)
	{
		___scoreValue_4 = value;
	}

	inline static int32_t get_offset_of_soundBonus_5() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___soundBonus_5)); }
	inline AudioClip_t3680889665 * get_soundBonus_5() const { return ___soundBonus_5; }
	inline AudioClip_t3680889665 ** get_address_of_soundBonus_5() { return &___soundBonus_5; }
	inline void set_soundBonus_5(AudioClip_t3680889665 * value)
	{
		___soundBonus_5 = value;
		Il2CppCodeGenWriteBarrier((&___soundBonus_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTBONUS_T461169207_H
#ifndef CRGTFADER_T912498268_H
#define CRGTFADER_T912498268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTFader
struct  CRGTFader_t912498268  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup CRGTFader::faderGroup
	CanvasGroup_t4083511760 * ___faderGroup_2;
	// System.Single CRGTFader::fadeTime
	float ___fadeTime_3;
	// System.Single CRGTFader::fadeFinal
	float ___fadeFinal_4;

public:
	inline static int32_t get_offset_of_faderGroup_2() { return static_cast<int32_t>(offsetof(CRGTFader_t912498268, ___faderGroup_2)); }
	inline CanvasGroup_t4083511760 * get_faderGroup_2() const { return ___faderGroup_2; }
	inline CanvasGroup_t4083511760 ** get_address_of_faderGroup_2() { return &___faderGroup_2; }
	inline void set_faderGroup_2(CanvasGroup_t4083511760 * value)
	{
		___faderGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&___faderGroup_2), value);
	}

	inline static int32_t get_offset_of_fadeTime_3() { return static_cast<int32_t>(offsetof(CRGTFader_t912498268, ___fadeTime_3)); }
	inline float get_fadeTime_3() const { return ___fadeTime_3; }
	inline float* get_address_of_fadeTime_3() { return &___fadeTime_3; }
	inline void set_fadeTime_3(float value)
	{
		___fadeTime_3 = value;
	}

	inline static int32_t get_offset_of_fadeFinal_4() { return static_cast<int32_t>(offsetof(CRGTFader_t912498268, ___fadeFinal_4)); }
	inline float get_fadeFinal_4() const { return ___fadeFinal_4; }
	inline float* get_address_of_fadeFinal_4() { return &___fadeFinal_4; }
	inline void set_fadeFinal_4(float value)
	{
		___fadeFinal_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTFADER_T912498268_H
#ifndef CRGTFPS_T1814840959_H
#define CRGTFPS_T1814840959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTFps
struct  CRGTFps_t1814840959  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTFps::updateInterval
	float ___updateInterval_2;
	// System.Single CRGTFps::accum
	float ___accum_3;
	// System.Int32 CRGTFps::frames
	int32_t ___frames_4;
	// System.Single CRGTFps::timeLeft
	float ___timeLeft_5;
	// UnityEngine.GUIText CRGTFps::FpsText
	GUIText_t402233326 * ___FpsText_6;

public:
	inline static int32_t get_offset_of_updateInterval_2() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___updateInterval_2)); }
	inline float get_updateInterval_2() const { return ___updateInterval_2; }
	inline float* get_address_of_updateInterval_2() { return &___updateInterval_2; }
	inline void set_updateInterval_2(float value)
	{
		___updateInterval_2 = value;
	}

	inline static int32_t get_offset_of_accum_3() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___accum_3)); }
	inline float get_accum_3() const { return ___accum_3; }
	inline float* get_address_of_accum_3() { return &___accum_3; }
	inline void set_accum_3(float value)
	{
		___accum_3 = value;
	}

	inline static int32_t get_offset_of_frames_4() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___frames_4)); }
	inline int32_t get_frames_4() const { return ___frames_4; }
	inline int32_t* get_address_of_frames_4() { return &___frames_4; }
	inline void set_frames_4(int32_t value)
	{
		___frames_4 = value;
	}

	inline static int32_t get_offset_of_timeLeft_5() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___timeLeft_5)); }
	inline float get_timeLeft_5() const { return ___timeLeft_5; }
	inline float* get_address_of_timeLeft_5() { return &___timeLeft_5; }
	inline void set_timeLeft_5(float value)
	{
		___timeLeft_5 = value;
	}

	inline static int32_t get_offset_of_FpsText_6() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___FpsText_6)); }
	inline GUIText_t402233326 * get_FpsText_6() const { return ___FpsText_6; }
	inline GUIText_t402233326 ** get_address_of_FpsText_6() { return &___FpsText_6; }
	inline void set_FpsText_6(GUIText_t402233326 * value)
	{
		___FpsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___FpsText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTFPS_T1814840959_H
#ifndef CRGTPLAYERCONTROLLER_T3312304059_H
#define CRGTPLAYERCONTROLLER_T3312304059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTPlayerController
struct  CRGTPlayerController_t3312304059  : public MonoBehaviour_t3962482529
{
public:
	// ControlType CRGTPlayerController::playerControlType
	int32_t ___playerControlType_2;
	// System.Single CRGTPlayerController::playerSideSpeed
	float ___playerSideSpeed_5;
	// System.Boolean CRGTPlayerController::movePlayerRot
	bool ___movePlayerRot_6;
	// System.Single CRGTPlayerController::playerRot
	float ___playerRot_7;
	// System.Single CRGTPlayerController::playerRotSpeed
	float ___playerRotSpeed_8;
	// System.Single[] CRGTPlayerController::spawnPlayerXPos
	SingleU5BU5D_t1444911251* ___spawnPlayerXPos_9;
	// System.Single CRGTPlayerController::playerMinX
	float ___playerMinX_10;
	// System.Single CRGTPlayerController::playerMaxX
	float ___playerMaxX_11;
	// UnityEngine.GameObject CRGTPlayerController::crashParticles
	GameObject_t1113636619 * ___crashParticles_12;
	// UnityEngine.GameObject CRGTPlayerController::turboEffect
	GameObject_t1113636619 * ___turboEffect_13;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboFlareL
	ParticleSystem_t1800779281 * ___turboFlareL_14;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboFlareR
	ParticleSystem_t1800779281 * ___turboFlareR_15;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboCoreL
	ParticleSystem_t1800779281 * ___turboCoreL_16;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboCoreR
	ParticleSystem_t1800779281 * ___turboCoreR_17;
	// System.Single CRGTPlayerController::rotationMultiplier
	float ___rotationMultiplier_18;
	// UnityEngine.AudioClip CRGTPlayerController::carCrash
	AudioClip_t3680889665 * ___carCrash_19;
	// System.Single CRGTPlayerController::playerPosY
	float ___playerPosY_20;
	// System.Single CRGTPlayerController::playerVelX
	float ___playerVelX_21;
	// UnityEngine.Rigidbody2D CRGTPlayerController::rigBody2D
	Rigidbody2D_t939494601 * ___rigBody2D_22;

public:
	inline static int32_t get_offset_of_playerControlType_2() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerControlType_2)); }
	inline int32_t get_playerControlType_2() const { return ___playerControlType_2; }
	inline int32_t* get_address_of_playerControlType_2() { return &___playerControlType_2; }
	inline void set_playerControlType_2(int32_t value)
	{
		___playerControlType_2 = value;
	}

	inline static int32_t get_offset_of_playerSideSpeed_5() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerSideSpeed_5)); }
	inline float get_playerSideSpeed_5() const { return ___playerSideSpeed_5; }
	inline float* get_address_of_playerSideSpeed_5() { return &___playerSideSpeed_5; }
	inline void set_playerSideSpeed_5(float value)
	{
		___playerSideSpeed_5 = value;
	}

	inline static int32_t get_offset_of_movePlayerRot_6() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___movePlayerRot_6)); }
	inline bool get_movePlayerRot_6() const { return ___movePlayerRot_6; }
	inline bool* get_address_of_movePlayerRot_6() { return &___movePlayerRot_6; }
	inline void set_movePlayerRot_6(bool value)
	{
		___movePlayerRot_6 = value;
	}

	inline static int32_t get_offset_of_playerRot_7() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerRot_7)); }
	inline float get_playerRot_7() const { return ___playerRot_7; }
	inline float* get_address_of_playerRot_7() { return &___playerRot_7; }
	inline void set_playerRot_7(float value)
	{
		___playerRot_7 = value;
	}

	inline static int32_t get_offset_of_playerRotSpeed_8() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerRotSpeed_8)); }
	inline float get_playerRotSpeed_8() const { return ___playerRotSpeed_8; }
	inline float* get_address_of_playerRotSpeed_8() { return &___playerRotSpeed_8; }
	inline void set_playerRotSpeed_8(float value)
	{
		___playerRotSpeed_8 = value;
	}

	inline static int32_t get_offset_of_spawnPlayerXPos_9() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___spawnPlayerXPos_9)); }
	inline SingleU5BU5D_t1444911251* get_spawnPlayerXPos_9() const { return ___spawnPlayerXPos_9; }
	inline SingleU5BU5D_t1444911251** get_address_of_spawnPlayerXPos_9() { return &___spawnPlayerXPos_9; }
	inline void set_spawnPlayerXPos_9(SingleU5BU5D_t1444911251* value)
	{
		___spawnPlayerXPos_9 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPlayerXPos_9), value);
	}

	inline static int32_t get_offset_of_playerMinX_10() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerMinX_10)); }
	inline float get_playerMinX_10() const { return ___playerMinX_10; }
	inline float* get_address_of_playerMinX_10() { return &___playerMinX_10; }
	inline void set_playerMinX_10(float value)
	{
		___playerMinX_10 = value;
	}

	inline static int32_t get_offset_of_playerMaxX_11() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerMaxX_11)); }
	inline float get_playerMaxX_11() const { return ___playerMaxX_11; }
	inline float* get_address_of_playerMaxX_11() { return &___playerMaxX_11; }
	inline void set_playerMaxX_11(float value)
	{
		___playerMaxX_11 = value;
	}

	inline static int32_t get_offset_of_crashParticles_12() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___crashParticles_12)); }
	inline GameObject_t1113636619 * get_crashParticles_12() const { return ___crashParticles_12; }
	inline GameObject_t1113636619 ** get_address_of_crashParticles_12() { return &___crashParticles_12; }
	inline void set_crashParticles_12(GameObject_t1113636619 * value)
	{
		___crashParticles_12 = value;
		Il2CppCodeGenWriteBarrier((&___crashParticles_12), value);
	}

	inline static int32_t get_offset_of_turboEffect_13() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboEffect_13)); }
	inline GameObject_t1113636619 * get_turboEffect_13() const { return ___turboEffect_13; }
	inline GameObject_t1113636619 ** get_address_of_turboEffect_13() { return &___turboEffect_13; }
	inline void set_turboEffect_13(GameObject_t1113636619 * value)
	{
		___turboEffect_13 = value;
		Il2CppCodeGenWriteBarrier((&___turboEffect_13), value);
	}

	inline static int32_t get_offset_of_turboFlareL_14() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboFlareL_14)); }
	inline ParticleSystem_t1800779281 * get_turboFlareL_14() const { return ___turboFlareL_14; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboFlareL_14() { return &___turboFlareL_14; }
	inline void set_turboFlareL_14(ParticleSystem_t1800779281 * value)
	{
		___turboFlareL_14 = value;
		Il2CppCodeGenWriteBarrier((&___turboFlareL_14), value);
	}

	inline static int32_t get_offset_of_turboFlareR_15() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboFlareR_15)); }
	inline ParticleSystem_t1800779281 * get_turboFlareR_15() const { return ___turboFlareR_15; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboFlareR_15() { return &___turboFlareR_15; }
	inline void set_turboFlareR_15(ParticleSystem_t1800779281 * value)
	{
		___turboFlareR_15 = value;
		Il2CppCodeGenWriteBarrier((&___turboFlareR_15), value);
	}

	inline static int32_t get_offset_of_turboCoreL_16() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboCoreL_16)); }
	inline ParticleSystem_t1800779281 * get_turboCoreL_16() const { return ___turboCoreL_16; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboCoreL_16() { return &___turboCoreL_16; }
	inline void set_turboCoreL_16(ParticleSystem_t1800779281 * value)
	{
		___turboCoreL_16 = value;
		Il2CppCodeGenWriteBarrier((&___turboCoreL_16), value);
	}

	inline static int32_t get_offset_of_turboCoreR_17() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboCoreR_17)); }
	inline ParticleSystem_t1800779281 * get_turboCoreR_17() const { return ___turboCoreR_17; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboCoreR_17() { return &___turboCoreR_17; }
	inline void set_turboCoreR_17(ParticleSystem_t1800779281 * value)
	{
		___turboCoreR_17 = value;
		Il2CppCodeGenWriteBarrier((&___turboCoreR_17), value);
	}

	inline static int32_t get_offset_of_rotationMultiplier_18() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___rotationMultiplier_18)); }
	inline float get_rotationMultiplier_18() const { return ___rotationMultiplier_18; }
	inline float* get_address_of_rotationMultiplier_18() { return &___rotationMultiplier_18; }
	inline void set_rotationMultiplier_18(float value)
	{
		___rotationMultiplier_18 = value;
	}

	inline static int32_t get_offset_of_carCrash_19() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___carCrash_19)); }
	inline AudioClip_t3680889665 * get_carCrash_19() const { return ___carCrash_19; }
	inline AudioClip_t3680889665 ** get_address_of_carCrash_19() { return &___carCrash_19; }
	inline void set_carCrash_19(AudioClip_t3680889665 * value)
	{
		___carCrash_19 = value;
		Il2CppCodeGenWriteBarrier((&___carCrash_19), value);
	}

	inline static int32_t get_offset_of_playerPosY_20() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerPosY_20)); }
	inline float get_playerPosY_20() const { return ___playerPosY_20; }
	inline float* get_address_of_playerPosY_20() { return &___playerPosY_20; }
	inline void set_playerPosY_20(float value)
	{
		___playerPosY_20 = value;
	}

	inline static int32_t get_offset_of_playerVelX_21() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerVelX_21)); }
	inline float get_playerVelX_21() const { return ___playerVelX_21; }
	inline float* get_address_of_playerVelX_21() { return &___playerVelX_21; }
	inline void set_playerVelX_21(float value)
	{
		___playerVelX_21 = value;
	}

	inline static int32_t get_offset_of_rigBody2D_22() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___rigBody2D_22)); }
	inline Rigidbody2D_t939494601 * get_rigBody2D_22() const { return ___rigBody2D_22; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigBody2D_22() { return &___rigBody2D_22; }
	inline void set_rigBody2D_22(Rigidbody2D_t939494601 * value)
	{
		___rigBody2D_22 = value;
		Il2CppCodeGenWriteBarrier((&___rigBody2D_22), value);
	}
};

struct CRGTPlayerController_t3312304059_StaticFields
{
public:
	// System.Boolean CRGTPlayerController::isAlive
	bool ___isAlive_3;
	// System.Single CRGTPlayerController::playerSpeed
	float ___playerSpeed_4;

public:
	inline static int32_t get_offset_of_isAlive_3() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059_StaticFields, ___isAlive_3)); }
	inline bool get_isAlive_3() const { return ___isAlive_3; }
	inline bool* get_address_of_isAlive_3() { return &___isAlive_3; }
	inline void set_isAlive_3(bool value)
	{
		___isAlive_3 = value;
	}

	inline static int32_t get_offset_of_playerSpeed_4() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059_StaticFields, ___playerSpeed_4)); }
	inline float get_playerSpeed_4() const { return ___playerSpeed_4; }
	inline float* get_address_of_playerSpeed_4() { return &___playerSpeed_4; }
	inline void set_playerSpeed_4(float value)
	{
		___playerSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTPLAYERCONTROLLER_T3312304059_H
#ifndef CRGTSPRITETOGGLE_T4126742132_H
#define CRGTSPRITETOGGLE_T4126742132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTSpriteToggle
struct  CRGTSpriteToggle_t4126742132  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite CRGTSpriteToggle::ImageOn
	Sprite_t280657092 * ___ImageOn_2;
	// UnityEngine.Sprite CRGTSpriteToggle::ImageOff
	Sprite_t280657092 * ___ImageOff_3;
	// UnityEngine.Sprite CRGTSpriteToggle::ImageLocked
	Sprite_t280657092 * ___ImageLocked_4;
	// UnityEngine.UI.Image CRGTSpriteToggle::targetImage
	Image_t2670269651 * ___targetImage_5;
	// System.Int32 CRGTSpriteToggle::toggleState
	int32_t ___toggleState_6;

public:
	inline static int32_t get_offset_of_ImageOn_2() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___ImageOn_2)); }
	inline Sprite_t280657092 * get_ImageOn_2() const { return ___ImageOn_2; }
	inline Sprite_t280657092 ** get_address_of_ImageOn_2() { return &___ImageOn_2; }
	inline void set_ImageOn_2(Sprite_t280657092 * value)
	{
		___ImageOn_2 = value;
		Il2CppCodeGenWriteBarrier((&___ImageOn_2), value);
	}

	inline static int32_t get_offset_of_ImageOff_3() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___ImageOff_3)); }
	inline Sprite_t280657092 * get_ImageOff_3() const { return ___ImageOff_3; }
	inline Sprite_t280657092 ** get_address_of_ImageOff_3() { return &___ImageOff_3; }
	inline void set_ImageOff_3(Sprite_t280657092 * value)
	{
		___ImageOff_3 = value;
		Il2CppCodeGenWriteBarrier((&___ImageOff_3), value);
	}

	inline static int32_t get_offset_of_ImageLocked_4() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___ImageLocked_4)); }
	inline Sprite_t280657092 * get_ImageLocked_4() const { return ___ImageLocked_4; }
	inline Sprite_t280657092 ** get_address_of_ImageLocked_4() { return &___ImageLocked_4; }
	inline void set_ImageLocked_4(Sprite_t280657092 * value)
	{
		___ImageLocked_4 = value;
		Il2CppCodeGenWriteBarrier((&___ImageLocked_4), value);
	}

	inline static int32_t get_offset_of_targetImage_5() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___targetImage_5)); }
	inline Image_t2670269651 * get_targetImage_5() const { return ___targetImage_5; }
	inline Image_t2670269651 ** get_address_of_targetImage_5() { return &___targetImage_5; }
	inline void set_targetImage_5(Image_t2670269651 * value)
	{
		___targetImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetImage_5), value);
	}

	inline static int32_t get_offset_of_toggleState_6() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___toggleState_6)); }
	inline int32_t get_toggleState_6() const { return ___toggleState_6; }
	inline int32_t* get_address_of_toggleState_6() { return &___toggleState_6; }
	inline void set_toggleState_6(int32_t value)
	{
		___toggleState_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTSPRITETOGGLE_T4126742132_H
#ifndef LANGCTRL_T2672849805_H
#define LANGCTRL_T2672849805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LangCtrl
struct  LangCtrl_t2672849805  : public MonoBehaviour_t3962482529
{
public:
	// System.String LangCtrl::m_ItemTxt
	String_t* ___m_ItemTxt_2;
	// UnityEngine.UI.Text LangCtrl::m_Text
	Text_t1901882714 * ___m_Text_3;
	// System.String LangCtrl::tmpStr
	String_t* ___tmpStr_4;

public:
	inline static int32_t get_offset_of_m_ItemTxt_2() { return static_cast<int32_t>(offsetof(LangCtrl_t2672849805, ___m_ItemTxt_2)); }
	inline String_t* get_m_ItemTxt_2() const { return ___m_ItemTxt_2; }
	inline String_t** get_address_of_m_ItemTxt_2() { return &___m_ItemTxt_2; }
	inline void set_m_ItemTxt_2(String_t* value)
	{
		___m_ItemTxt_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemTxt_2), value);
	}

	inline static int32_t get_offset_of_m_Text_3() { return static_cast<int32_t>(offsetof(LangCtrl_t2672849805, ___m_Text_3)); }
	inline Text_t1901882714 * get_m_Text_3() const { return ___m_Text_3; }
	inline Text_t1901882714 ** get_address_of_m_Text_3() { return &___m_Text_3; }
	inline void set_m_Text_3(Text_t1901882714 * value)
	{
		___m_Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_3), value);
	}

	inline static int32_t get_offset_of_tmpStr_4() { return static_cast<int32_t>(offsetof(LangCtrl_t2672849805, ___tmpStr_4)); }
	inline String_t* get_tmpStr_4() const { return ___tmpStr_4; }
	inline String_t** get_address_of_tmpStr_4() { return &___tmpStr_4; }
	inline void set_tmpStr_4(String_t* value)
	{
		___tmpStr_4 = value;
		Il2CppCodeGenWriteBarrier((&___tmpStr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGCTRL_T2672849805_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef CRGTREMOVEAFTERTIME_T3231471983_H
#define CRGTREMOVEAFTERTIME_T3231471983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTRemoveAfterTime
struct  CRGTRemoveAfterTime_t3231471983  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTRemoveAfterTime::removeAfterTime
	float ___removeAfterTime_2;

public:
	inline static int32_t get_offset_of_removeAfterTime_2() { return static_cast<int32_t>(offsetof(CRGTRemoveAfterTime_t3231471983, ___removeAfterTime_2)); }
	inline float get_removeAfterTime_2() const { return ___removeAfterTime_2; }
	inline float* get_address_of_removeAfterTime_2() { return &___removeAfterTime_2; }
	inline void set_removeAfterTime_2(float value)
	{
		___removeAfterTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTREMOVEAFTERTIME_T3231471983_H
#ifndef CRGTSOUNDMANAGER_T434062966_H
#define CRGTSOUNDMANAGER_T434062966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTSoundManager
struct  CRGTSoundManager_t434062966  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource CRGTSoundManager::efxSource
	AudioSource_t3935305588 * ___efxSource_3;
	// UnityEngine.AudioSource CRGTSoundManager::musicSource
	AudioSource_t3935305588 * ___musicSource_4;
	// CRGTSpriteToggle[] CRGTSoundManager::Buttons
	CRGTSpriteToggleU5BU5D_t2845023549* ___Buttons_5;
	// System.Single CRGTSoundManager::currentState
	float ___currentState_6;
	// System.Boolean CRGTSoundManager::mute
	bool ___mute_7;

public:
	inline static int32_t get_offset_of_efxSource_3() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___efxSource_3)); }
	inline AudioSource_t3935305588 * get_efxSource_3() const { return ___efxSource_3; }
	inline AudioSource_t3935305588 ** get_address_of_efxSource_3() { return &___efxSource_3; }
	inline void set_efxSource_3(AudioSource_t3935305588 * value)
	{
		___efxSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___efxSource_3), value);
	}

	inline static int32_t get_offset_of_musicSource_4() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___musicSource_4)); }
	inline AudioSource_t3935305588 * get_musicSource_4() const { return ___musicSource_4; }
	inline AudioSource_t3935305588 ** get_address_of_musicSource_4() { return &___musicSource_4; }
	inline void set_musicSource_4(AudioSource_t3935305588 * value)
	{
		___musicSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___musicSource_4), value);
	}

	inline static int32_t get_offset_of_Buttons_5() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___Buttons_5)); }
	inline CRGTSpriteToggleU5BU5D_t2845023549* get_Buttons_5() const { return ___Buttons_5; }
	inline CRGTSpriteToggleU5BU5D_t2845023549** get_address_of_Buttons_5() { return &___Buttons_5; }
	inline void set_Buttons_5(CRGTSpriteToggleU5BU5D_t2845023549* value)
	{
		___Buttons_5 = value;
		Il2CppCodeGenWriteBarrier((&___Buttons_5), value);
	}

	inline static int32_t get_offset_of_currentState_6() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___currentState_6)); }
	inline float get_currentState_6() const { return ___currentState_6; }
	inline float* get_address_of_currentState_6() { return &___currentState_6; }
	inline void set_currentState_6(float value)
	{
		___currentState_6 = value;
	}

	inline static int32_t get_offset_of_mute_7() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___mute_7)); }
	inline bool get_mute_7() const { return ___mute_7; }
	inline bool* get_address_of_mute_7() { return &___mute_7; }
	inline void set_mute_7(bool value)
	{
		___mute_7 = value;
	}
};

struct CRGTSoundManager_t434062966_StaticFields
{
public:
	// CRGTSoundManager CRGTSoundManager::instance
	CRGTSoundManager_t434062966 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966_StaticFields, ___instance_2)); }
	inline CRGTSoundManager_t434062966 * get_instance_2() const { return ___instance_2; }
	inline CRGTSoundManager_t434062966 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CRGTSoundManager_t434062966 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTSOUNDMANAGER_T434062966_H
#ifndef CRGTREMOVEONTOUCH_T2211313123_H
#define CRGTREMOVEONTOUCH_T2211313123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTRemoveOnTouch
struct  CRGTRemoveOnTouch_t2211313123  : public MonoBehaviour_t3962482529
{
public:
	// System.String CRGTRemoveOnTouch::TargetTag
	String_t* ___TargetTag_2;
	// System.Boolean CRGTRemoveOnTouch::removeTarget
	bool ___removeTarget_3;
	// System.Single CRGTRemoveOnTouch::removeTargetTime
	float ___removeTargetTime_4;
	// System.Boolean CRGTRemoveOnTouch::removeSelf
	bool ___removeSelf_5;
	// System.Single CRGTRemoveOnTouch::removeSelfTime
	float ___removeSelfTime_6;

public:
	inline static int32_t get_offset_of_TargetTag_2() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___TargetTag_2)); }
	inline String_t* get_TargetTag_2() const { return ___TargetTag_2; }
	inline String_t** get_address_of_TargetTag_2() { return &___TargetTag_2; }
	inline void set_TargetTag_2(String_t* value)
	{
		___TargetTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTag_2), value);
	}

	inline static int32_t get_offset_of_removeTarget_3() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeTarget_3)); }
	inline bool get_removeTarget_3() const { return ___removeTarget_3; }
	inline bool* get_address_of_removeTarget_3() { return &___removeTarget_3; }
	inline void set_removeTarget_3(bool value)
	{
		___removeTarget_3 = value;
	}

	inline static int32_t get_offset_of_removeTargetTime_4() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeTargetTime_4)); }
	inline float get_removeTargetTime_4() const { return ___removeTargetTime_4; }
	inline float* get_address_of_removeTargetTime_4() { return &___removeTargetTime_4; }
	inline void set_removeTargetTime_4(float value)
	{
		___removeTargetTime_4 = value;
	}

	inline static int32_t get_offset_of_removeSelf_5() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeSelf_5)); }
	inline bool get_removeSelf_5() const { return ___removeSelf_5; }
	inline bool* get_address_of_removeSelf_5() { return &___removeSelf_5; }
	inline void set_removeSelf_5(bool value)
	{
		___removeSelf_5 = value;
	}

	inline static int32_t get_offset_of_removeSelfTime_6() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeSelfTime_6)); }
	inline float get_removeSelfTime_6() const { return ___removeSelfTime_6; }
	inline float* get_address_of_removeSelfTime_6() { return &___removeSelfTime_6; }
	inline void set_removeSelfTime_6(float value)
	{
		___removeSelfTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTREMOVEONTOUCH_T2211313123_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_2)); }
	inline RectOffset_t1369453676 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t1369453676 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_6)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_9)); }
	inline List_1_t881764471 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t881764471 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t729725570  : public LayoutGroup_t2436138090
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H
#ifndef VERTICALLAYOUTGROUP_T923838031_H
#define VERTICALLAYOUTGROUP_T923838031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t923838031  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T923838031_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (LayoutGroup_t2436138090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[8] = 
{
	LayoutGroup_t2436138090::get_offset_of_m_Padding_2(),
	LayoutGroup_t2436138090::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t2436138090::get_offset_of_m_Rect_4(),
	LayoutGroup_t2436138090::get_offset_of_m_Tracker_5(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t2436138090::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1802[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1803[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1809[7] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1816[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1824[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (CRGTBonus_t461169207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[4] = 
{
	CRGTBonus_t461169207::get_offset_of_scoreEffect_2(),
	CRGTBonus_t461169207::get_offset_of_bonusParticles_3(),
	CRGTBonus_t461169207::get_offset_of_scoreValue_4(),
	CRGTBonus_t461169207::get_offset_of_soundBonus_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (CRGTFader_t912498268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[3] = 
{
	CRGTFader_t912498268::get_offset_of_faderGroup_2(),
	CRGTFader_t912498268::get_offset_of_fadeTime_3(),
	CRGTFader_t912498268::get_offset_of_fadeFinal_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (U3CStartFadeU3Ec__Iterator0_t3292164234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[7] = 
{
	U3CStartFadeU3Ec__Iterator0_t3292164234::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CStartFadeU3Ec__Iterator0_t3292164234::get_offset_of_U3CwaitU3E__0_1(),
	U3CStartFadeU3Ec__Iterator0_t3292164234::get_offset_of_fade_2(),
	U3CStartFadeU3Ec__Iterator0_t3292164234::get_offset_of_U24this_3(),
	U3CStartFadeU3Ec__Iterator0_t3292164234::get_offset_of_U24current_4(),
	U3CStartFadeU3Ec__Iterator0_t3292164234::get_offset_of_U24disposing_5(),
	U3CStartFadeU3Ec__Iterator0_t3292164234::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (CRGTFps_t1814840959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[5] = 
{
	CRGTFps_t1814840959::get_offset_of_updateInterval_2(),
	CRGTFps_t1814840959::get_offset_of_accum_3(),
	CRGTFps_t1814840959::get_offset_of_frames_4(),
	CRGTFps_t1814840959::get_offset_of_timeLeft_5(),
	CRGTFps_t1814840959::get_offset_of_FpsText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (CRGTGameManager_t3284010685), -1, sizeof(CRGTGameManager_t3284010685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1831[33] = 
{
	CRGTGameManager_t3284010685_StaticFields::get_offset_of_instance_2(),
	CRGTGameManager_t3284010685::get_offset_of_startGameSpeed_3(),
	CRGTGameManager_t3284010685::get_offset_of_gameSpeed_4(),
	CRGTGameManager_t3284010685::get_offset_of_highGameScore_5(),
	CRGTGameManager_t3284010685::get_offset_of_lastHighGameScore_6(),
	CRGTGameManager_t3284010685::get_offset_of_lastGameScore_7(),
	CRGTGameManager_t3284010685::get_offset_of_bonusGameCount_8(),
	CRGTGameManager_t3284010685::get_offset_of_spawnTime_9(),
	CRGTGameManager_t3284010685::get_offset_of_spawnSpeed_10(),
	CRGTGameManager_t3284010685::get_offset_of_isGameOver_11(),
	CRGTGameManager_t3284010685::get_offset_of_isGamePaused_12(),
	CRGTGameManager_t3284010685::get_offset_of_gameScore_13(),
	CRGTGameManager_t3284010685::get_offset_of_startSpawnSpeed_14(),
	CRGTGameManager_t3284010685::get_offset_of_spawnStep_15(),
	CRGTGameManager_t3284010685::get_offset_of_minSpawSpeed_16(),
	CRGTGameManager_t3284010685::get_offset_of_spawnLine_17(),
	CRGTGameManager_t3284010685::get_offset_of_spawnObjectsXPos_18(),
	CRGTGameManager_t3284010685::get_offset_of_spawnGameObjects_19(),
	CRGTGameManager_t3284010685::get_offset_of_spawnObject_20(),
	CRGTGameManager_t3284010685::get_offset_of_buttonClick_21(),
	CRGTGameManager_t3284010685::get_offset_of_gameScoreText_22(),
	CRGTGameManager_t3284010685::get_offset_of_gameBestScoreText_23(),
	CRGTGameManager_t3284010685::get_offset_of_gameLastScoreText_24(),
	CRGTGameManager_t3284010685::get_offset_of_bonusGameCountText_25(),
	CRGTGameManager_t3284010685::get_offset_of_gameOverScoreText_26(),
	CRGTGameManager_t3284010685::get_offset_of_gameOverNewText_27(),
	CRGTGameManager_t3284010685::get_offset_of_gameOverHighScoreText_28(),
	CRGTGameManager_t3284010685::get_offset_of_gameOverBonusCountText_29(),
	CRGTGameManager_t3284010685::get_offset_of_menuCanvas_30(),
	CRGTGameManager_t3284010685::get_offset_of_gameCanvas_31(),
	CRGTGameManager_t3284010685::get_offset_of_pauseCanvas_32(),
	CRGTGameManager_t3284010685::get_offset_of_gameOverCanvas_33(),
	CRGTGameManager_t3284010685::get_offset_of_gameOverURL_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (CRGTMaterialMover_t3899902404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[4] = 
{
	CRGTMaterialMover_t3899902404::get_offset_of_materialSpeedX_2(),
	CRGTMaterialMover_t3899902404::get_offset_of_materialSpeedY_3(),
	CRGTMaterialMover_t3899902404::get_offset_of_materialOffset_4(),
	CRGTMaterialMover_t3899902404::get_offset_of_materialRenderer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (CRGTObjectDestroyer_t475906085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[4] = 
{
	CRGTObjectDestroyer_t475906085::get_offset_of_removeTarget_2(),
	CRGTObjectDestroyer_t475906085::get_offset_of_removeTargetTime_3(),
	CRGTObjectDestroyer_t475906085::get_offset_of_removeSelf_4(),
	CRGTObjectDestroyer_t475906085::get_offset_of_removeSelfTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (CRGTObjectMover_t1733178804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[4] = 
{
	CRGTObjectMover_t1733178804::get_offset_of_objectSpeed_2(),
	CRGTObjectMover_t1733178804::get_offset_of_objectMoveX_3(),
	CRGTObjectMover_t1733178804::get_offset_of_objectMoveY_4(),
	CRGTObjectMover_t1733178804::get_offset_of_objectMoveZ_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (ControlType_t166092394)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1835[3] = 
{
	ControlType_t166092394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (CRGTPlayerController_t3312304059), -1, sizeof(CRGTPlayerController_t3312304059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[21] = 
{
	CRGTPlayerController_t3312304059::get_offset_of_playerControlType_2(),
	CRGTPlayerController_t3312304059_StaticFields::get_offset_of_isAlive_3(),
	CRGTPlayerController_t3312304059_StaticFields::get_offset_of_playerSpeed_4(),
	CRGTPlayerController_t3312304059::get_offset_of_playerSideSpeed_5(),
	CRGTPlayerController_t3312304059::get_offset_of_movePlayerRot_6(),
	CRGTPlayerController_t3312304059::get_offset_of_playerRot_7(),
	CRGTPlayerController_t3312304059::get_offset_of_playerRotSpeed_8(),
	CRGTPlayerController_t3312304059::get_offset_of_spawnPlayerXPos_9(),
	CRGTPlayerController_t3312304059::get_offset_of_playerMinX_10(),
	CRGTPlayerController_t3312304059::get_offset_of_playerMaxX_11(),
	CRGTPlayerController_t3312304059::get_offset_of_crashParticles_12(),
	CRGTPlayerController_t3312304059::get_offset_of_turboEffect_13(),
	CRGTPlayerController_t3312304059::get_offset_of_turboFlareL_14(),
	CRGTPlayerController_t3312304059::get_offset_of_turboFlareR_15(),
	CRGTPlayerController_t3312304059::get_offset_of_turboCoreL_16(),
	CRGTPlayerController_t3312304059::get_offset_of_turboCoreR_17(),
	CRGTPlayerController_t3312304059::get_offset_of_rotationMultiplier_18(),
	CRGTPlayerController_t3312304059::get_offset_of_carCrash_19(),
	CRGTPlayerController_t3312304059::get_offset_of_playerPosY_20(),
	CRGTPlayerController_t3312304059::get_offset_of_playerVelX_21(),
	CRGTPlayerController_t3312304059::get_offset_of_rigBody2D_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (CRGTRemoveAfterTime_t3231471983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[1] = 
{
	CRGTRemoveAfterTime_t3231471983::get_offset_of_removeAfterTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (CRGTRemoveOnTouch_t2211313123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[5] = 
{
	CRGTRemoveOnTouch_t2211313123::get_offset_of_TargetTag_2(),
	CRGTRemoveOnTouch_t2211313123::get_offset_of_removeTarget_3(),
	CRGTRemoveOnTouch_t2211313123::get_offset_of_removeTargetTime_4(),
	CRGTRemoveOnTouch_t2211313123::get_offset_of_removeSelf_5(),
	CRGTRemoveOnTouch_t2211313123::get_offset_of_removeSelfTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (CRGTSoundManager_t434062966), -1, sizeof(CRGTSoundManager_t434062966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1839[6] = 
{
	CRGTSoundManager_t434062966_StaticFields::get_offset_of_instance_2(),
	CRGTSoundManager_t434062966::get_offset_of_efxSource_3(),
	CRGTSoundManager_t434062966::get_offset_of_musicSource_4(),
	CRGTSoundManager_t434062966::get_offset_of_Buttons_5(),
	CRGTSoundManager_t434062966::get_offset_of_currentState_6(),
	CRGTSoundManager_t434062966::get_offset_of_mute_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (CRGTSpriteToggle_t4126742132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[5] = 
{
	CRGTSpriteToggle_t4126742132::get_offset_of_ImageOn_2(),
	CRGTSpriteToggle_t4126742132::get_offset_of_ImageOff_3(),
	CRGTSpriteToggle_t4126742132::get_offset_of_ImageLocked_4(),
	CRGTSpriteToggle_t4126742132::get_offset_of_targetImage_5(),
	CRGTSpriteToggle_t4126742132::get_offset_of_toggleState_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (GameMode_t1892292441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1841[5] = 
{
	GameMode_t1892292441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (GB_t166534947), -1, sizeof(GB_t166534947_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1842[3] = 
{
	GB_t166534947_StaticFields::get_offset_of_m_GameMode_0(),
	GB_t166534947_StaticFields::get_offset_of_g_GameScore_1(),
	GB_t166534947_StaticFields::get_offset_of_g_LangType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (LangCtrl_t2672849805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[3] = 
{
	LangCtrl_t2672849805::get_offset_of_m_ItemTxt_2(),
	LangCtrl_t2672849805::get_offset_of_m_Text_3(),
	LangCtrl_t2672849805::get_offset_of_tmpStr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (U24ArrayTypeU3D16_t3253128244)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128244 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif

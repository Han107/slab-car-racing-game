﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// CRGTBonus
struct CRGTBonus_t461169207;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Component
struct Component_t1923634451;
// System.String
struct String_t;
// CRGTGameManager
struct CRGTGameManager_t3284010685;
// UnityEngine.Object
struct Object_t631007953;
// CRGTSoundManager
struct CRGTSoundManager_t434062966;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Text
struct Text_t1901882714;
// CRGTFader
struct CRGTFader_t912498268;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// CRGTFader/<StartFade>c__Iterator0
struct U3CStartFadeU3Ec__Iterator0_t3292164234;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// CRGTFps
struct CRGTFps_t1814840959;
// UnityEngine.GUIText
struct GUIText_t402233326;
// CRGTMaterialMover
struct CRGTMaterialMover_t3899902404;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Material
struct Material_t340375123;
// CRGTObjectDestroyer
struct CRGTObjectDestroyer_t475906085;
// CRGTObjectMover
struct CRGTObjectMover_t1733178804;
// CRGTPlayerController
struct CRGTPlayerController_t3312304059;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// CRGTRemoveAfterTime
struct CRGTRemoveAfterTime_t3231471983;
// CRGTRemoveOnTouch
struct CRGTRemoveOnTouch_t2211313123;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// CRGTSpriteToggle
struct CRGTSpriteToggle_t4126742132;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// GB
struct GB_t166534947;
// LangCtrl
struct LangCtrl_t2672849805;
// UnityEngine.TextAsset
struct TextAsset_t3022178571;
// LitJson.JsonData
struct JsonData_t1524858407;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1677636661;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// System.Collections.Generic.IList`1<LitJson.JsonData>
struct IList_1_t3340178190;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>
struct IDictionary_2_t4068933393;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IList_1_t1228139360;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// CRGTSpriteToggle[]
struct CRGTSpriteToggleU5BU5D_t2845023549;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;

extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* CRGTGameManager_t3284010685_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* CRGTSoundManager_t434062966_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisTransform_t3600365921_m1831857851_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var;
extern String_t* _stringLiteral2261822918;
extern String_t* _stringLiteral3987835886;
extern String_t* _stringLiteral3452614533;
extern const uint32_t CRGTBonus_OnTriggerEnter2D_m1670428559_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentInChildren_TisCanvasGroup_t4083511760_m2486821986_RuntimeMethod_var;
extern const uint32_t CRGTFader_Start_m864434492_MetadataUsageId;
extern const uint32_t CRGTFader_OnEnable_m3391193346_MetadataUsageId;
extern RuntimeClass* U3CStartFadeU3Ec__Iterator0_t3292164234_il2cpp_TypeInfo_var;
extern const uint32_t CRGTFader_StartFade_m4220097985_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CStartFadeU3Ec__Iterator0_Reset_m3269543568_RuntimeMethod_var;
extern const uint32_t U3CStartFadeU3Ec__Iterator0_Reset_m3269543568_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisGUIText_t402233326_m4001847390_RuntimeMethod_var;
extern const uint32_t CRGTFps_Start_m3597135823_MetadataUsageId;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral4010238652;
extern const uint32_t CRGTFps_Update_m3736617682_MetadataUsageId;
extern RuntimeClass* SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255366____U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0_FieldInfo_var;
extern String_t* _stringLiteral400424176;
extern const uint32_t CRGTGameManager__ctor_m2891170590_MetadataUsageId;
extern const uint32_t CRGTGameManager_Awake_m2006082390_MetadataUsageId;
extern const uint32_t CRGTGameManager_SpawnNewObject_m3505301815_MetadataUsageId;
extern const uint32_t CRGTGameManager_CleanUpScene_m4197824408_MetadataUsageId;
extern String_t* _stringLiteral16531513;
extern String_t* _stringLiteral4048657555;
extern String_t* _stringLiteral537924729;
extern const uint32_t CRGTGameManager_LoadGameData_m380892872_MetadataUsageId;
extern const uint32_t CRGTGameManager_SaveGameData_m1501793627_MetadataUsageId;
extern String_t* _stringLiteral727109017;
extern const uint32_t CRGTGameManager_GameOver_m3937302641_MetadataUsageId;
extern const uint32_t CRGTGameManager_ButtonSound_m513884379_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var;
extern const uint32_t CRGTMaterialMover_Start_m3587889192_MetadataUsageId;
extern const uint32_t CRGTMaterialMover_Update_m311836227_MetadataUsageId;
extern String_t* _stringLiteral1390680557;
extern const uint32_t CRGTObjectDestroyer_OnTriggerEnter2D_m3103412951_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t CRGTObjectMover_Update_m2405874294_MetadataUsageId;
extern const uint32_t CRGTPlayerController__ctor_m2467363767_MetadataUsageId;
extern RuntimeClass* CRGTPlayerController_t3312304059_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var;
extern const uint32_t CRGTPlayerController_Start_m3101612879_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t CRGTPlayerController_FixedUpdate_m90202351_MetadataUsageId;
extern String_t* _stringLiteral1581558434;
extern const uint32_t CRGTPlayerController_OnTriggerEnter2D_m1499976993_MetadataUsageId;
extern const uint32_t CRGTPlayerController__cctor_m1370838780_MetadataUsageId;
extern const uint32_t CRGTRemoveAfterTime_Start_m163819186_MetadataUsageId;
extern String_t* _stringLiteral3076559204;
extern const uint32_t CRGTRemoveOnTouch__ctor_m746880976_MetadataUsageId;
extern const uint32_t CRGTRemoveOnTouch_OnTriggerEnter2D_m480148688_MetadataUsageId;
extern const uint32_t CRGTSoundManager_Awake_m2042567216_MetadataUsageId;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1691373009;
extern String_t* _stringLiteral534994172;
extern String_t* _stringLiteral353759352;
extern const uint32_t CRGTSoundManager_UpdateVolume_m617425764_MetadataUsageId;
extern const uint32_t CRGTSoundManager_SetSoundOnOff_m376341164_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const uint32_t CRGTSpriteToggle_Start_m2686693449_MetadataUsageId;
extern const uint32_t CRGTSpriteToggle_SetToggleValue_m212693942_MetadataUsageId;
extern RuntimeClass* GB_t166534947_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3455301613;
extern const uint32_t GB__cctor_m595848670_MetadataUsageId;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* JsonMapper_t3815285241_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Resources_Load_TisTextAsset_t3022178571_m724759995_RuntimeMethod_var;
extern String_t* _stringLiteral184291000;
extern String_t* _stringLiteral3244380236;
extern String_t* _stringLiteral931865376;
extern const uint32_t LangCtrl_SetLang_m691616426_MetadataUsageId;
extern String_t* _stringLiteral3456153565;
extern String_t* _stringLiteral3454646236;
extern String_t* _stringLiteral3454842843;
extern String_t* _stringLiteral3455170522;
extern String_t* _stringLiteral3455629274;
extern String_t* _stringLiteral3454384089;
extern String_t* _stringLiteral3455629273;
extern String_t* _stringLiteral3455760358;
extern String_t* _stringLiteral3454777317;
extern String_t* _stringLiteral3454777314;
extern String_t* _stringLiteral3454973936;
extern String_t* _stringLiteral3454777328;
extern String_t* _stringLiteral3454777326;
extern String_t* _stringLiteral3455432686;
extern String_t* _stringLiteral3455760365;
extern const uint32_t LangCtrl_CheckLangType_m87832360_MetadataUsageId;

struct SingleU5BU5D_t1444911251;
struct GameObjectU5BU5D_t3328599146;
struct CRGTSpriteToggleU5BU5D_t2845023549;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef U3CSTARTFADEU3EC__ITERATOR0_T3292164234_H
#define U3CSTARTFADEU3EC__ITERATOR0_T3292164234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTFader/<StartFade>c__Iterator0
struct  U3CStartFadeU3Ec__Iterator0_t3292164234  : public RuntimeObject
{
public:
	// System.Single CRGTFader/<StartFade>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// System.Single CRGTFader/<StartFade>c__Iterator0::<wait>__0
	float ___U3CwaitU3E__0_1;
	// System.Boolean CRGTFader/<StartFade>c__Iterator0::fade
	bool ___fade_2;
	// CRGTFader CRGTFader/<StartFade>c__Iterator0::$this
	CRGTFader_t912498268 * ___U24this_3;
	// System.Object CRGTFader/<StartFade>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean CRGTFader/<StartFade>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 CRGTFader/<StartFade>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwaitU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U3CwaitU3E__0_1)); }
	inline float get_U3CwaitU3E__0_1() const { return ___U3CwaitU3E__0_1; }
	inline float* get_address_of_U3CwaitU3E__0_1() { return &___U3CwaitU3E__0_1; }
	inline void set_U3CwaitU3E__0_1(float value)
	{
		___U3CwaitU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_fade_2() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___fade_2)); }
	inline bool get_fade_2() const { return ___fade_2; }
	inline bool* get_address_of_fade_2() { return &___fade_2; }
	inline void set_fade_2(bool value)
	{
		___fade_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24this_3)); }
	inline CRGTFader_t912498268 * get_U24this_3() const { return ___U24this_3; }
	inline CRGTFader_t912498268 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(CRGTFader_t912498268 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartFadeU3Ec__Iterator0_t3292164234, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFADEU3EC__ITERATOR0_T3292164234_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-AC9373DEC6917FF369D56F004DADC890EAA43D80
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0;

public:
	inline static int32_t get_offset_of_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0() const { return ___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0() { return &___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0; }
	inline void set_U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef SYSTEMLANGUAGE_T949212163_H
#define SYSTEMLANGUAGE_T949212163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemLanguage
struct  SystemLanguage_t949212163 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SystemLanguage_t949212163, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMLANGUAGE_T949212163_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef GAMEMODE_T1892292441_H
#define GAMEMODE_T1892292441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMode
struct  GameMode_t1892292441 
{
public:
	// System.Int32 GameMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameMode_t1892292441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMODE_T1892292441_H
#ifndef RUNTIMEFIELDHANDLE_T1871169219_H
#define RUNTIMEFIELDHANDLE_T1871169219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1871169219 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1871169219, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1871169219_H
#ifndef JSONTYPE_T2731125707_H
#define JSONTYPE_T2731125707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonType
struct  JsonType_t2731125707 
{
public:
	// System.Int32 LitJson.JsonType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonType_t2731125707, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPE_T2731125707_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CONTROLTYPE_T166092394_H
#define CONTROLTYPE_T166092394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlType
struct  ControlType_t166092394 
{
public:
	// System.Int32 ControlType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlType_t166092394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTYPE_T166092394_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef AUDIOCLIP_T3680889665_H
#define AUDIOCLIP_T3680889665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t3680889665  : public Object_t631007953
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1677636661 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t1059417452 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t1677636661 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t1677636661 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t1677636661 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t1059417452 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t1059417452 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t1059417452 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T3680889665_H
#ifndef GB_T166534947_H
#define GB_T166534947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GB
struct  GB_t166534947  : public RuntimeObject
{
public:

public:
};

struct GB_t166534947_StaticFields
{
public:
	// GameMode GB::m_GameMode
	int32_t ___m_GameMode_0;
	// System.Int32 GB::g_GameScore
	int32_t ___g_GameScore_1;
	// System.String GB::g_LangType
	String_t* ___g_LangType_2;

public:
	inline static int32_t get_offset_of_m_GameMode_0() { return static_cast<int32_t>(offsetof(GB_t166534947_StaticFields, ___m_GameMode_0)); }
	inline int32_t get_m_GameMode_0() const { return ___m_GameMode_0; }
	inline int32_t* get_address_of_m_GameMode_0() { return &___m_GameMode_0; }
	inline void set_m_GameMode_0(int32_t value)
	{
		___m_GameMode_0 = value;
	}

	inline static int32_t get_offset_of_g_GameScore_1() { return static_cast<int32_t>(offsetof(GB_t166534947_StaticFields, ___g_GameScore_1)); }
	inline int32_t get_g_GameScore_1() const { return ___g_GameScore_1; }
	inline int32_t* get_address_of_g_GameScore_1() { return &___g_GameScore_1; }
	inline void set_g_GameScore_1(int32_t value)
	{
		___g_GameScore_1 = value;
	}

	inline static int32_t get_offset_of_g_LangType_2() { return static_cast<int32_t>(offsetof(GB_t166534947_StaticFields, ___g_LangType_2)); }
	inline String_t* get_g_LangType_2() const { return ___g_LangType_2; }
	inline String_t** get_address_of_g_LangType_2() { return &___g_LangType_2; }
	inline void set_g_LangType_2(String_t* value)
	{
		___g_LangType_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_LangType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GB_T166534947_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef JSONDATA_T1524858407_H
#define JSONDATA_T1524858407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonData
struct  JsonData_t1524858407  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<LitJson.JsonData> LitJson.JsonData::inst_array
	RuntimeObject* ___inst_array_0;
	// System.Boolean LitJson.JsonData::inst_boolean
	bool ___inst_boolean_1;
	// System.Double LitJson.JsonData::inst_double
	double ___inst_double_2;
	// System.Int32 LitJson.JsonData::inst_int
	int32_t ___inst_int_3;
	// System.Int64 LitJson.JsonData::inst_long
	int64_t ___inst_long_4;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData> LitJson.JsonData::inst_object
	RuntimeObject* ___inst_object_5;
	// System.String LitJson.JsonData::inst_string
	String_t* ___inst_string_6;
	// System.String LitJson.JsonData::json
	String_t* ___json_7;
	// LitJson.JsonType LitJson.JsonData::type
	int32_t ___type_8;
	// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.JsonData::object_list
	RuntimeObject* ___object_list_9;

public:
	inline static int32_t get_offset_of_inst_array_0() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_array_0)); }
	inline RuntimeObject* get_inst_array_0() const { return ___inst_array_0; }
	inline RuntimeObject** get_address_of_inst_array_0() { return &___inst_array_0; }
	inline void set_inst_array_0(RuntimeObject* value)
	{
		___inst_array_0 = value;
		Il2CppCodeGenWriteBarrier((&___inst_array_0), value);
	}

	inline static int32_t get_offset_of_inst_boolean_1() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_boolean_1)); }
	inline bool get_inst_boolean_1() const { return ___inst_boolean_1; }
	inline bool* get_address_of_inst_boolean_1() { return &___inst_boolean_1; }
	inline void set_inst_boolean_1(bool value)
	{
		___inst_boolean_1 = value;
	}

	inline static int32_t get_offset_of_inst_double_2() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_double_2)); }
	inline double get_inst_double_2() const { return ___inst_double_2; }
	inline double* get_address_of_inst_double_2() { return &___inst_double_2; }
	inline void set_inst_double_2(double value)
	{
		___inst_double_2 = value;
	}

	inline static int32_t get_offset_of_inst_int_3() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_int_3)); }
	inline int32_t get_inst_int_3() const { return ___inst_int_3; }
	inline int32_t* get_address_of_inst_int_3() { return &___inst_int_3; }
	inline void set_inst_int_3(int32_t value)
	{
		___inst_int_3 = value;
	}

	inline static int32_t get_offset_of_inst_long_4() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_long_4)); }
	inline int64_t get_inst_long_4() const { return ___inst_long_4; }
	inline int64_t* get_address_of_inst_long_4() { return &___inst_long_4; }
	inline void set_inst_long_4(int64_t value)
	{
		___inst_long_4 = value;
	}

	inline static int32_t get_offset_of_inst_object_5() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_object_5)); }
	inline RuntimeObject* get_inst_object_5() const { return ___inst_object_5; }
	inline RuntimeObject** get_address_of_inst_object_5() { return &___inst_object_5; }
	inline void set_inst_object_5(RuntimeObject* value)
	{
		___inst_object_5 = value;
		Il2CppCodeGenWriteBarrier((&___inst_object_5), value);
	}

	inline static int32_t get_offset_of_inst_string_6() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___inst_string_6)); }
	inline String_t* get_inst_string_6() const { return ___inst_string_6; }
	inline String_t** get_address_of_inst_string_6() { return &___inst_string_6; }
	inline void set_inst_string_6(String_t* value)
	{
		___inst_string_6 = value;
		Il2CppCodeGenWriteBarrier((&___inst_string_6), value);
	}

	inline static int32_t get_offset_of_json_7() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___json_7)); }
	inline String_t* get_json_7() const { return ___json_7; }
	inline String_t** get_address_of_json_7() { return &___json_7; }
	inline void set_json_7(String_t* value)
	{
		___json_7 = value;
		Il2CppCodeGenWriteBarrier((&___json_7), value);
	}

	inline static int32_t get_offset_of_type_8() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___type_8)); }
	inline int32_t get_type_8() const { return ___type_8; }
	inline int32_t* get_address_of_type_8() { return &___type_8; }
	inline void set_type_8(int32_t value)
	{
		___type_8 = value;
	}

	inline static int32_t get_offset_of_object_list_9() { return static_cast<int32_t>(offsetof(JsonData_t1524858407, ___object_list_9)); }
	inline RuntimeObject* get_object_list_9() const { return ___object_list_9; }
	inline RuntimeObject** get_address_of_object_list_9() { return &___object_list_9; }
	inline void set_object_list_9(RuntimeObject* value)
	{
		___object_list_9 = value;
		Il2CppCodeGenWriteBarrier((&___object_list_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDATA_T1524858407_H
#ifndef TEXTASSET_T3022178571_H
#define TEXTASSET_T3022178571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAsset
struct  TextAsset_t3022178571  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTASSET_T3022178571_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CANVASGROUP_T4083511760_H
#define CANVASGROUP_T4083511760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t4083511760  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T4083511760_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef GUIELEMENT_T3567083079_H
#define GUIELEMENT_T3567083079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t3567083079  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T3567083079_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public Behaviour_t1437897464
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GUITEXT_T402233326_H
#define GUITEXT_T402233326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIText
struct  GUIText_t402233326  : public GUIElement_t3567083079
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXT_T402233326_H
#ifndef CRGTFPS_T1814840959_H
#define CRGTFPS_T1814840959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTFps
struct  CRGTFps_t1814840959  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTFps::updateInterval
	float ___updateInterval_2;
	// System.Single CRGTFps::accum
	float ___accum_3;
	// System.Int32 CRGTFps::frames
	int32_t ___frames_4;
	// System.Single CRGTFps::timeLeft
	float ___timeLeft_5;
	// UnityEngine.GUIText CRGTFps::FpsText
	GUIText_t402233326 * ___FpsText_6;

public:
	inline static int32_t get_offset_of_updateInterval_2() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___updateInterval_2)); }
	inline float get_updateInterval_2() const { return ___updateInterval_2; }
	inline float* get_address_of_updateInterval_2() { return &___updateInterval_2; }
	inline void set_updateInterval_2(float value)
	{
		___updateInterval_2 = value;
	}

	inline static int32_t get_offset_of_accum_3() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___accum_3)); }
	inline float get_accum_3() const { return ___accum_3; }
	inline float* get_address_of_accum_3() { return &___accum_3; }
	inline void set_accum_3(float value)
	{
		___accum_3 = value;
	}

	inline static int32_t get_offset_of_frames_4() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___frames_4)); }
	inline int32_t get_frames_4() const { return ___frames_4; }
	inline int32_t* get_address_of_frames_4() { return &___frames_4; }
	inline void set_frames_4(int32_t value)
	{
		___frames_4 = value;
	}

	inline static int32_t get_offset_of_timeLeft_5() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___timeLeft_5)); }
	inline float get_timeLeft_5() const { return ___timeLeft_5; }
	inline float* get_address_of_timeLeft_5() { return &___timeLeft_5; }
	inline void set_timeLeft_5(float value)
	{
		___timeLeft_5 = value;
	}

	inline static int32_t get_offset_of_FpsText_6() { return static_cast<int32_t>(offsetof(CRGTFps_t1814840959, ___FpsText_6)); }
	inline GUIText_t402233326 * get_FpsText_6() const { return ___FpsText_6; }
	inline GUIText_t402233326 ** get_address_of_FpsText_6() { return &___FpsText_6; }
	inline void set_FpsText_6(GUIText_t402233326 * value)
	{
		___FpsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___FpsText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTFPS_T1814840959_H
#ifndef CRGTOBJECTDESTROYER_T475906085_H
#define CRGTOBJECTDESTROYER_T475906085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTObjectDestroyer
struct  CRGTObjectDestroyer_t475906085  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CRGTObjectDestroyer::removeTarget
	bool ___removeTarget_2;
	// System.Single CRGTObjectDestroyer::removeTargetTime
	float ___removeTargetTime_3;
	// System.Boolean CRGTObjectDestroyer::removeSelf
	bool ___removeSelf_4;
	// System.Single CRGTObjectDestroyer::removeSelfTime
	float ___removeSelfTime_5;

public:
	inline static int32_t get_offset_of_removeTarget_2() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeTarget_2)); }
	inline bool get_removeTarget_2() const { return ___removeTarget_2; }
	inline bool* get_address_of_removeTarget_2() { return &___removeTarget_2; }
	inline void set_removeTarget_2(bool value)
	{
		___removeTarget_2 = value;
	}

	inline static int32_t get_offset_of_removeTargetTime_3() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeTargetTime_3)); }
	inline float get_removeTargetTime_3() const { return ___removeTargetTime_3; }
	inline float* get_address_of_removeTargetTime_3() { return &___removeTargetTime_3; }
	inline void set_removeTargetTime_3(float value)
	{
		___removeTargetTime_3 = value;
	}

	inline static int32_t get_offset_of_removeSelf_4() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeSelf_4)); }
	inline bool get_removeSelf_4() const { return ___removeSelf_4; }
	inline bool* get_address_of_removeSelf_4() { return &___removeSelf_4; }
	inline void set_removeSelf_4(bool value)
	{
		___removeSelf_4 = value;
	}

	inline static int32_t get_offset_of_removeSelfTime_5() { return static_cast<int32_t>(offsetof(CRGTObjectDestroyer_t475906085, ___removeSelfTime_5)); }
	inline float get_removeSelfTime_5() const { return ___removeSelfTime_5; }
	inline float* get_address_of_removeSelfTime_5() { return &___removeSelfTime_5; }
	inline void set_removeSelfTime_5(float value)
	{
		___removeSelfTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTOBJECTDESTROYER_T475906085_H
#ifndef CRGTMATERIALMOVER_T3899902404_H
#define CRGTMATERIALMOVER_T3899902404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTMaterialMover
struct  CRGTMaterialMover_t3899902404  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTMaterialMover::materialSpeedX
	float ___materialSpeedX_2;
	// System.Single CRGTMaterialMover::materialSpeedY
	float ___materialSpeedY_3;
	// UnityEngine.Vector2 CRGTMaterialMover::materialOffset
	Vector2_t2156229523  ___materialOffset_4;
	// UnityEngine.Renderer CRGTMaterialMover::materialRenderer
	Renderer_t2627027031 * ___materialRenderer_5;

public:
	inline static int32_t get_offset_of_materialSpeedX_2() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialSpeedX_2)); }
	inline float get_materialSpeedX_2() const { return ___materialSpeedX_2; }
	inline float* get_address_of_materialSpeedX_2() { return &___materialSpeedX_2; }
	inline void set_materialSpeedX_2(float value)
	{
		___materialSpeedX_2 = value;
	}

	inline static int32_t get_offset_of_materialSpeedY_3() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialSpeedY_3)); }
	inline float get_materialSpeedY_3() const { return ___materialSpeedY_3; }
	inline float* get_address_of_materialSpeedY_3() { return &___materialSpeedY_3; }
	inline void set_materialSpeedY_3(float value)
	{
		___materialSpeedY_3 = value;
	}

	inline static int32_t get_offset_of_materialOffset_4() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialOffset_4)); }
	inline Vector2_t2156229523  get_materialOffset_4() const { return ___materialOffset_4; }
	inline Vector2_t2156229523 * get_address_of_materialOffset_4() { return &___materialOffset_4; }
	inline void set_materialOffset_4(Vector2_t2156229523  value)
	{
		___materialOffset_4 = value;
	}

	inline static int32_t get_offset_of_materialRenderer_5() { return static_cast<int32_t>(offsetof(CRGTMaterialMover_t3899902404, ___materialRenderer_5)); }
	inline Renderer_t2627027031 * get_materialRenderer_5() const { return ___materialRenderer_5; }
	inline Renderer_t2627027031 ** get_address_of_materialRenderer_5() { return &___materialRenderer_5; }
	inline void set_materialRenderer_5(Renderer_t2627027031 * value)
	{
		___materialRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___materialRenderer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTMATERIALMOVER_T3899902404_H
#ifndef CRGTGAMEMANAGER_T3284010685_H
#define CRGTGAMEMANAGER_T3284010685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTGameManager
struct  CRGTGameManager_t3284010685  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTGameManager::startGameSpeed
	float ___startGameSpeed_3;
	// System.Single CRGTGameManager::gameSpeed
	float ___gameSpeed_4;
	// System.Int32 CRGTGameManager::highGameScore
	int32_t ___highGameScore_5;
	// System.Int32 CRGTGameManager::lastHighGameScore
	int32_t ___lastHighGameScore_6;
	// System.Int32 CRGTGameManager::lastGameScore
	int32_t ___lastGameScore_7;
	// System.Int32 CRGTGameManager::bonusGameCount
	int32_t ___bonusGameCount_8;
	// System.Single CRGTGameManager::spawnTime
	float ___spawnTime_9;
	// System.Single CRGTGameManager::spawnSpeed
	float ___spawnSpeed_10;
	// System.Boolean CRGTGameManager::isGameOver
	bool ___isGameOver_11;
	// System.Boolean CRGTGameManager::isGamePaused
	bool ___isGamePaused_12;
	// System.Int32 CRGTGameManager::gameScore
	int32_t ___gameScore_13;
	// System.Single CRGTGameManager::startSpawnSpeed
	float ___startSpawnSpeed_14;
	// System.Single CRGTGameManager::spawnStep
	float ___spawnStep_15;
	// System.Single CRGTGameManager::minSpawSpeed
	float ___minSpawSpeed_16;
	// UnityEngine.RectTransform CRGTGameManager::spawnLine
	RectTransform_t3704657025 * ___spawnLine_17;
	// System.Single[] CRGTGameManager::spawnObjectsXPos
	SingleU5BU5D_t1444911251* ___spawnObjectsXPos_18;
	// UnityEngine.GameObject[] CRGTGameManager::spawnGameObjects
	GameObjectU5BU5D_t3328599146* ___spawnGameObjects_19;
	// UnityEngine.GameObject CRGTGameManager::spawnObject
	GameObject_t1113636619 * ___spawnObject_20;
	// UnityEngine.AudioClip CRGTGameManager::buttonClick
	AudioClip_t3680889665 * ___buttonClick_21;
	// UnityEngine.UI.Text CRGTGameManager::gameScoreText
	Text_t1901882714 * ___gameScoreText_22;
	// UnityEngine.UI.Text CRGTGameManager::gameBestScoreText
	Text_t1901882714 * ___gameBestScoreText_23;
	// UnityEngine.UI.Text CRGTGameManager::gameLastScoreText
	Text_t1901882714 * ___gameLastScoreText_24;
	// UnityEngine.UI.Text CRGTGameManager::bonusGameCountText
	Text_t1901882714 * ___bonusGameCountText_25;
	// UnityEngine.UI.Text CRGTGameManager::gameOverScoreText
	Text_t1901882714 * ___gameOverScoreText_26;
	// UnityEngine.UI.Text CRGTGameManager::gameOverNewText
	Text_t1901882714 * ___gameOverNewText_27;
	// UnityEngine.UI.Text CRGTGameManager::gameOverHighScoreText
	Text_t1901882714 * ___gameOverHighScoreText_28;
	// UnityEngine.UI.Text CRGTGameManager::gameOverBonusCountText
	Text_t1901882714 * ___gameOverBonusCountText_29;
	// UnityEngine.GameObject CRGTGameManager::menuCanvas
	GameObject_t1113636619 * ___menuCanvas_30;
	// UnityEngine.GameObject CRGTGameManager::gameCanvas
	GameObject_t1113636619 * ___gameCanvas_31;
	// UnityEngine.GameObject CRGTGameManager::pauseCanvas
	GameObject_t1113636619 * ___pauseCanvas_32;
	// UnityEngine.GameObject CRGTGameManager::gameOverCanvas
	GameObject_t1113636619 * ___gameOverCanvas_33;
	// System.String CRGTGameManager::gameOverURL
	String_t* ___gameOverURL_34;

public:
	inline static int32_t get_offset_of_startGameSpeed_3() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___startGameSpeed_3)); }
	inline float get_startGameSpeed_3() const { return ___startGameSpeed_3; }
	inline float* get_address_of_startGameSpeed_3() { return &___startGameSpeed_3; }
	inline void set_startGameSpeed_3(float value)
	{
		___startGameSpeed_3 = value;
	}

	inline static int32_t get_offset_of_gameSpeed_4() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameSpeed_4)); }
	inline float get_gameSpeed_4() const { return ___gameSpeed_4; }
	inline float* get_address_of_gameSpeed_4() { return &___gameSpeed_4; }
	inline void set_gameSpeed_4(float value)
	{
		___gameSpeed_4 = value;
	}

	inline static int32_t get_offset_of_highGameScore_5() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___highGameScore_5)); }
	inline int32_t get_highGameScore_5() const { return ___highGameScore_5; }
	inline int32_t* get_address_of_highGameScore_5() { return &___highGameScore_5; }
	inline void set_highGameScore_5(int32_t value)
	{
		___highGameScore_5 = value;
	}

	inline static int32_t get_offset_of_lastHighGameScore_6() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___lastHighGameScore_6)); }
	inline int32_t get_lastHighGameScore_6() const { return ___lastHighGameScore_6; }
	inline int32_t* get_address_of_lastHighGameScore_6() { return &___lastHighGameScore_6; }
	inline void set_lastHighGameScore_6(int32_t value)
	{
		___lastHighGameScore_6 = value;
	}

	inline static int32_t get_offset_of_lastGameScore_7() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___lastGameScore_7)); }
	inline int32_t get_lastGameScore_7() const { return ___lastGameScore_7; }
	inline int32_t* get_address_of_lastGameScore_7() { return &___lastGameScore_7; }
	inline void set_lastGameScore_7(int32_t value)
	{
		___lastGameScore_7 = value;
	}

	inline static int32_t get_offset_of_bonusGameCount_8() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___bonusGameCount_8)); }
	inline int32_t get_bonusGameCount_8() const { return ___bonusGameCount_8; }
	inline int32_t* get_address_of_bonusGameCount_8() { return &___bonusGameCount_8; }
	inline void set_bonusGameCount_8(int32_t value)
	{
		___bonusGameCount_8 = value;
	}

	inline static int32_t get_offset_of_spawnTime_9() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnTime_9)); }
	inline float get_spawnTime_9() const { return ___spawnTime_9; }
	inline float* get_address_of_spawnTime_9() { return &___spawnTime_9; }
	inline void set_spawnTime_9(float value)
	{
		___spawnTime_9 = value;
	}

	inline static int32_t get_offset_of_spawnSpeed_10() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnSpeed_10)); }
	inline float get_spawnSpeed_10() const { return ___spawnSpeed_10; }
	inline float* get_address_of_spawnSpeed_10() { return &___spawnSpeed_10; }
	inline void set_spawnSpeed_10(float value)
	{
		___spawnSpeed_10 = value;
	}

	inline static int32_t get_offset_of_isGameOver_11() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___isGameOver_11)); }
	inline bool get_isGameOver_11() const { return ___isGameOver_11; }
	inline bool* get_address_of_isGameOver_11() { return &___isGameOver_11; }
	inline void set_isGameOver_11(bool value)
	{
		___isGameOver_11 = value;
	}

	inline static int32_t get_offset_of_isGamePaused_12() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___isGamePaused_12)); }
	inline bool get_isGamePaused_12() const { return ___isGamePaused_12; }
	inline bool* get_address_of_isGamePaused_12() { return &___isGamePaused_12; }
	inline void set_isGamePaused_12(bool value)
	{
		___isGamePaused_12 = value;
	}

	inline static int32_t get_offset_of_gameScore_13() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameScore_13)); }
	inline int32_t get_gameScore_13() const { return ___gameScore_13; }
	inline int32_t* get_address_of_gameScore_13() { return &___gameScore_13; }
	inline void set_gameScore_13(int32_t value)
	{
		___gameScore_13 = value;
	}

	inline static int32_t get_offset_of_startSpawnSpeed_14() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___startSpawnSpeed_14)); }
	inline float get_startSpawnSpeed_14() const { return ___startSpawnSpeed_14; }
	inline float* get_address_of_startSpawnSpeed_14() { return &___startSpawnSpeed_14; }
	inline void set_startSpawnSpeed_14(float value)
	{
		___startSpawnSpeed_14 = value;
	}

	inline static int32_t get_offset_of_spawnStep_15() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnStep_15)); }
	inline float get_spawnStep_15() const { return ___spawnStep_15; }
	inline float* get_address_of_spawnStep_15() { return &___spawnStep_15; }
	inline void set_spawnStep_15(float value)
	{
		___spawnStep_15 = value;
	}

	inline static int32_t get_offset_of_minSpawSpeed_16() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___minSpawSpeed_16)); }
	inline float get_minSpawSpeed_16() const { return ___minSpawSpeed_16; }
	inline float* get_address_of_minSpawSpeed_16() { return &___minSpawSpeed_16; }
	inline void set_minSpawSpeed_16(float value)
	{
		___minSpawSpeed_16 = value;
	}

	inline static int32_t get_offset_of_spawnLine_17() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnLine_17)); }
	inline RectTransform_t3704657025 * get_spawnLine_17() const { return ___spawnLine_17; }
	inline RectTransform_t3704657025 ** get_address_of_spawnLine_17() { return &___spawnLine_17; }
	inline void set_spawnLine_17(RectTransform_t3704657025 * value)
	{
		___spawnLine_17 = value;
		Il2CppCodeGenWriteBarrier((&___spawnLine_17), value);
	}

	inline static int32_t get_offset_of_spawnObjectsXPos_18() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnObjectsXPos_18)); }
	inline SingleU5BU5D_t1444911251* get_spawnObjectsXPos_18() const { return ___spawnObjectsXPos_18; }
	inline SingleU5BU5D_t1444911251** get_address_of_spawnObjectsXPos_18() { return &___spawnObjectsXPos_18; }
	inline void set_spawnObjectsXPos_18(SingleU5BU5D_t1444911251* value)
	{
		___spawnObjectsXPos_18 = value;
		Il2CppCodeGenWriteBarrier((&___spawnObjectsXPos_18), value);
	}

	inline static int32_t get_offset_of_spawnGameObjects_19() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnGameObjects_19)); }
	inline GameObjectU5BU5D_t3328599146* get_spawnGameObjects_19() const { return ___spawnGameObjects_19; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_spawnGameObjects_19() { return &___spawnGameObjects_19; }
	inline void set_spawnGameObjects_19(GameObjectU5BU5D_t3328599146* value)
	{
		___spawnGameObjects_19 = value;
		Il2CppCodeGenWriteBarrier((&___spawnGameObjects_19), value);
	}

	inline static int32_t get_offset_of_spawnObject_20() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___spawnObject_20)); }
	inline GameObject_t1113636619 * get_spawnObject_20() const { return ___spawnObject_20; }
	inline GameObject_t1113636619 ** get_address_of_spawnObject_20() { return &___spawnObject_20; }
	inline void set_spawnObject_20(GameObject_t1113636619 * value)
	{
		___spawnObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___spawnObject_20), value);
	}

	inline static int32_t get_offset_of_buttonClick_21() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___buttonClick_21)); }
	inline AudioClip_t3680889665 * get_buttonClick_21() const { return ___buttonClick_21; }
	inline AudioClip_t3680889665 ** get_address_of_buttonClick_21() { return &___buttonClick_21; }
	inline void set_buttonClick_21(AudioClip_t3680889665 * value)
	{
		___buttonClick_21 = value;
		Il2CppCodeGenWriteBarrier((&___buttonClick_21), value);
	}

	inline static int32_t get_offset_of_gameScoreText_22() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameScoreText_22)); }
	inline Text_t1901882714 * get_gameScoreText_22() const { return ___gameScoreText_22; }
	inline Text_t1901882714 ** get_address_of_gameScoreText_22() { return &___gameScoreText_22; }
	inline void set_gameScoreText_22(Text_t1901882714 * value)
	{
		___gameScoreText_22 = value;
		Il2CppCodeGenWriteBarrier((&___gameScoreText_22), value);
	}

	inline static int32_t get_offset_of_gameBestScoreText_23() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameBestScoreText_23)); }
	inline Text_t1901882714 * get_gameBestScoreText_23() const { return ___gameBestScoreText_23; }
	inline Text_t1901882714 ** get_address_of_gameBestScoreText_23() { return &___gameBestScoreText_23; }
	inline void set_gameBestScoreText_23(Text_t1901882714 * value)
	{
		___gameBestScoreText_23 = value;
		Il2CppCodeGenWriteBarrier((&___gameBestScoreText_23), value);
	}

	inline static int32_t get_offset_of_gameLastScoreText_24() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameLastScoreText_24)); }
	inline Text_t1901882714 * get_gameLastScoreText_24() const { return ___gameLastScoreText_24; }
	inline Text_t1901882714 ** get_address_of_gameLastScoreText_24() { return &___gameLastScoreText_24; }
	inline void set_gameLastScoreText_24(Text_t1901882714 * value)
	{
		___gameLastScoreText_24 = value;
		Il2CppCodeGenWriteBarrier((&___gameLastScoreText_24), value);
	}

	inline static int32_t get_offset_of_bonusGameCountText_25() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___bonusGameCountText_25)); }
	inline Text_t1901882714 * get_bonusGameCountText_25() const { return ___bonusGameCountText_25; }
	inline Text_t1901882714 ** get_address_of_bonusGameCountText_25() { return &___bonusGameCountText_25; }
	inline void set_bonusGameCountText_25(Text_t1901882714 * value)
	{
		___bonusGameCountText_25 = value;
		Il2CppCodeGenWriteBarrier((&___bonusGameCountText_25), value);
	}

	inline static int32_t get_offset_of_gameOverScoreText_26() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverScoreText_26)); }
	inline Text_t1901882714 * get_gameOverScoreText_26() const { return ___gameOverScoreText_26; }
	inline Text_t1901882714 ** get_address_of_gameOverScoreText_26() { return &___gameOverScoreText_26; }
	inline void set_gameOverScoreText_26(Text_t1901882714 * value)
	{
		___gameOverScoreText_26 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScoreText_26), value);
	}

	inline static int32_t get_offset_of_gameOverNewText_27() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverNewText_27)); }
	inline Text_t1901882714 * get_gameOverNewText_27() const { return ___gameOverNewText_27; }
	inline Text_t1901882714 ** get_address_of_gameOverNewText_27() { return &___gameOverNewText_27; }
	inline void set_gameOverNewText_27(Text_t1901882714 * value)
	{
		___gameOverNewText_27 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverNewText_27), value);
	}

	inline static int32_t get_offset_of_gameOverHighScoreText_28() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverHighScoreText_28)); }
	inline Text_t1901882714 * get_gameOverHighScoreText_28() const { return ___gameOverHighScoreText_28; }
	inline Text_t1901882714 ** get_address_of_gameOverHighScoreText_28() { return &___gameOverHighScoreText_28; }
	inline void set_gameOverHighScoreText_28(Text_t1901882714 * value)
	{
		___gameOverHighScoreText_28 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverHighScoreText_28), value);
	}

	inline static int32_t get_offset_of_gameOverBonusCountText_29() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverBonusCountText_29)); }
	inline Text_t1901882714 * get_gameOverBonusCountText_29() const { return ___gameOverBonusCountText_29; }
	inline Text_t1901882714 ** get_address_of_gameOverBonusCountText_29() { return &___gameOverBonusCountText_29; }
	inline void set_gameOverBonusCountText_29(Text_t1901882714 * value)
	{
		___gameOverBonusCountText_29 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverBonusCountText_29), value);
	}

	inline static int32_t get_offset_of_menuCanvas_30() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___menuCanvas_30)); }
	inline GameObject_t1113636619 * get_menuCanvas_30() const { return ___menuCanvas_30; }
	inline GameObject_t1113636619 ** get_address_of_menuCanvas_30() { return &___menuCanvas_30; }
	inline void set_menuCanvas_30(GameObject_t1113636619 * value)
	{
		___menuCanvas_30 = value;
		Il2CppCodeGenWriteBarrier((&___menuCanvas_30), value);
	}

	inline static int32_t get_offset_of_gameCanvas_31() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameCanvas_31)); }
	inline GameObject_t1113636619 * get_gameCanvas_31() const { return ___gameCanvas_31; }
	inline GameObject_t1113636619 ** get_address_of_gameCanvas_31() { return &___gameCanvas_31; }
	inline void set_gameCanvas_31(GameObject_t1113636619 * value)
	{
		___gameCanvas_31 = value;
		Il2CppCodeGenWriteBarrier((&___gameCanvas_31), value);
	}

	inline static int32_t get_offset_of_pauseCanvas_32() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___pauseCanvas_32)); }
	inline GameObject_t1113636619 * get_pauseCanvas_32() const { return ___pauseCanvas_32; }
	inline GameObject_t1113636619 ** get_address_of_pauseCanvas_32() { return &___pauseCanvas_32; }
	inline void set_pauseCanvas_32(GameObject_t1113636619 * value)
	{
		___pauseCanvas_32 = value;
		Il2CppCodeGenWriteBarrier((&___pauseCanvas_32), value);
	}

	inline static int32_t get_offset_of_gameOverCanvas_33() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverCanvas_33)); }
	inline GameObject_t1113636619 * get_gameOverCanvas_33() const { return ___gameOverCanvas_33; }
	inline GameObject_t1113636619 ** get_address_of_gameOverCanvas_33() { return &___gameOverCanvas_33; }
	inline void set_gameOverCanvas_33(GameObject_t1113636619 * value)
	{
		___gameOverCanvas_33 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverCanvas_33), value);
	}

	inline static int32_t get_offset_of_gameOverURL_34() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685, ___gameOverURL_34)); }
	inline String_t* get_gameOverURL_34() const { return ___gameOverURL_34; }
	inline String_t** get_address_of_gameOverURL_34() { return &___gameOverURL_34; }
	inline void set_gameOverURL_34(String_t* value)
	{
		___gameOverURL_34 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverURL_34), value);
	}
};

struct CRGTGameManager_t3284010685_StaticFields
{
public:
	// CRGTGameManager CRGTGameManager::instance
	CRGTGameManager_t3284010685 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CRGTGameManager_t3284010685_StaticFields, ___instance_2)); }
	inline CRGTGameManager_t3284010685 * get_instance_2() const { return ___instance_2; }
	inline CRGTGameManager_t3284010685 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CRGTGameManager_t3284010685 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTGAMEMANAGER_T3284010685_H
#ifndef CRGTBONUS_T461169207_H
#define CRGTBONUS_T461169207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTBonus
struct  CRGTBonus_t461169207  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CRGTBonus::scoreEffect
	Transform_t3600365921 * ___scoreEffect_2;
	// UnityEngine.GameObject CRGTBonus::bonusParticles
	GameObject_t1113636619 * ___bonusParticles_3;
	// System.Int32 CRGTBonus::scoreValue
	int32_t ___scoreValue_4;
	// UnityEngine.AudioClip CRGTBonus::soundBonus
	AudioClip_t3680889665 * ___soundBonus_5;

public:
	inline static int32_t get_offset_of_scoreEffect_2() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___scoreEffect_2)); }
	inline Transform_t3600365921 * get_scoreEffect_2() const { return ___scoreEffect_2; }
	inline Transform_t3600365921 ** get_address_of_scoreEffect_2() { return &___scoreEffect_2; }
	inline void set_scoreEffect_2(Transform_t3600365921 * value)
	{
		___scoreEffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___scoreEffect_2), value);
	}

	inline static int32_t get_offset_of_bonusParticles_3() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___bonusParticles_3)); }
	inline GameObject_t1113636619 * get_bonusParticles_3() const { return ___bonusParticles_3; }
	inline GameObject_t1113636619 ** get_address_of_bonusParticles_3() { return &___bonusParticles_3; }
	inline void set_bonusParticles_3(GameObject_t1113636619 * value)
	{
		___bonusParticles_3 = value;
		Il2CppCodeGenWriteBarrier((&___bonusParticles_3), value);
	}

	inline static int32_t get_offset_of_scoreValue_4() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___scoreValue_4)); }
	inline int32_t get_scoreValue_4() const { return ___scoreValue_4; }
	inline int32_t* get_address_of_scoreValue_4() { return &___scoreValue_4; }
	inline void set_scoreValue_4(int32_t value)
	{
		___scoreValue_4 = value;
	}

	inline static int32_t get_offset_of_soundBonus_5() { return static_cast<int32_t>(offsetof(CRGTBonus_t461169207, ___soundBonus_5)); }
	inline AudioClip_t3680889665 * get_soundBonus_5() const { return ___soundBonus_5; }
	inline AudioClip_t3680889665 ** get_address_of_soundBonus_5() { return &___soundBonus_5; }
	inline void set_soundBonus_5(AudioClip_t3680889665 * value)
	{
		___soundBonus_5 = value;
		Il2CppCodeGenWriteBarrier((&___soundBonus_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTBONUS_T461169207_H
#ifndef CRGTFADER_T912498268_H
#define CRGTFADER_T912498268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTFader
struct  CRGTFader_t912498268  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup CRGTFader::faderGroup
	CanvasGroup_t4083511760 * ___faderGroup_2;
	// System.Single CRGTFader::fadeTime
	float ___fadeTime_3;
	// System.Single CRGTFader::fadeFinal
	float ___fadeFinal_4;

public:
	inline static int32_t get_offset_of_faderGroup_2() { return static_cast<int32_t>(offsetof(CRGTFader_t912498268, ___faderGroup_2)); }
	inline CanvasGroup_t4083511760 * get_faderGroup_2() const { return ___faderGroup_2; }
	inline CanvasGroup_t4083511760 ** get_address_of_faderGroup_2() { return &___faderGroup_2; }
	inline void set_faderGroup_2(CanvasGroup_t4083511760 * value)
	{
		___faderGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&___faderGroup_2), value);
	}

	inline static int32_t get_offset_of_fadeTime_3() { return static_cast<int32_t>(offsetof(CRGTFader_t912498268, ___fadeTime_3)); }
	inline float get_fadeTime_3() const { return ___fadeTime_3; }
	inline float* get_address_of_fadeTime_3() { return &___fadeTime_3; }
	inline void set_fadeTime_3(float value)
	{
		___fadeTime_3 = value;
	}

	inline static int32_t get_offset_of_fadeFinal_4() { return static_cast<int32_t>(offsetof(CRGTFader_t912498268, ___fadeFinal_4)); }
	inline float get_fadeFinal_4() const { return ___fadeFinal_4; }
	inline float* get_address_of_fadeFinal_4() { return &___fadeFinal_4; }
	inline void set_fadeFinal_4(float value)
	{
		___fadeFinal_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTFADER_T912498268_H
#ifndef CRGTSOUNDMANAGER_T434062966_H
#define CRGTSOUNDMANAGER_T434062966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTSoundManager
struct  CRGTSoundManager_t434062966  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource CRGTSoundManager::efxSource
	AudioSource_t3935305588 * ___efxSource_3;
	// UnityEngine.AudioSource CRGTSoundManager::musicSource
	AudioSource_t3935305588 * ___musicSource_4;
	// CRGTSpriteToggle[] CRGTSoundManager::Buttons
	CRGTSpriteToggleU5BU5D_t2845023549* ___Buttons_5;
	// System.Single CRGTSoundManager::currentState
	float ___currentState_6;
	// System.Boolean CRGTSoundManager::mute
	bool ___mute_7;

public:
	inline static int32_t get_offset_of_efxSource_3() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___efxSource_3)); }
	inline AudioSource_t3935305588 * get_efxSource_3() const { return ___efxSource_3; }
	inline AudioSource_t3935305588 ** get_address_of_efxSource_3() { return &___efxSource_3; }
	inline void set_efxSource_3(AudioSource_t3935305588 * value)
	{
		___efxSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___efxSource_3), value);
	}

	inline static int32_t get_offset_of_musicSource_4() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___musicSource_4)); }
	inline AudioSource_t3935305588 * get_musicSource_4() const { return ___musicSource_4; }
	inline AudioSource_t3935305588 ** get_address_of_musicSource_4() { return &___musicSource_4; }
	inline void set_musicSource_4(AudioSource_t3935305588 * value)
	{
		___musicSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___musicSource_4), value);
	}

	inline static int32_t get_offset_of_Buttons_5() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___Buttons_5)); }
	inline CRGTSpriteToggleU5BU5D_t2845023549* get_Buttons_5() const { return ___Buttons_5; }
	inline CRGTSpriteToggleU5BU5D_t2845023549** get_address_of_Buttons_5() { return &___Buttons_5; }
	inline void set_Buttons_5(CRGTSpriteToggleU5BU5D_t2845023549* value)
	{
		___Buttons_5 = value;
		Il2CppCodeGenWriteBarrier((&___Buttons_5), value);
	}

	inline static int32_t get_offset_of_currentState_6() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___currentState_6)); }
	inline float get_currentState_6() const { return ___currentState_6; }
	inline float* get_address_of_currentState_6() { return &___currentState_6; }
	inline void set_currentState_6(float value)
	{
		___currentState_6 = value;
	}

	inline static int32_t get_offset_of_mute_7() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966, ___mute_7)); }
	inline bool get_mute_7() const { return ___mute_7; }
	inline bool* get_address_of_mute_7() { return &___mute_7; }
	inline void set_mute_7(bool value)
	{
		___mute_7 = value;
	}
};

struct CRGTSoundManager_t434062966_StaticFields
{
public:
	// CRGTSoundManager CRGTSoundManager::instance
	CRGTSoundManager_t434062966 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CRGTSoundManager_t434062966_StaticFields, ___instance_2)); }
	inline CRGTSoundManager_t434062966 * get_instance_2() const { return ___instance_2; }
	inline CRGTSoundManager_t434062966 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CRGTSoundManager_t434062966 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTSOUNDMANAGER_T434062966_H
#ifndef CRGTREMOVEAFTERTIME_T3231471983_H
#define CRGTREMOVEAFTERTIME_T3231471983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTRemoveAfterTime
struct  CRGTRemoveAfterTime_t3231471983  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTRemoveAfterTime::removeAfterTime
	float ___removeAfterTime_2;

public:
	inline static int32_t get_offset_of_removeAfterTime_2() { return static_cast<int32_t>(offsetof(CRGTRemoveAfterTime_t3231471983, ___removeAfterTime_2)); }
	inline float get_removeAfterTime_2() const { return ___removeAfterTime_2; }
	inline float* get_address_of_removeAfterTime_2() { return &___removeAfterTime_2; }
	inline void set_removeAfterTime_2(float value)
	{
		___removeAfterTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTREMOVEAFTERTIME_T3231471983_H
#ifndef LANGCTRL_T2672849805_H
#define LANGCTRL_T2672849805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LangCtrl
struct  LangCtrl_t2672849805  : public MonoBehaviour_t3962482529
{
public:
	// System.String LangCtrl::m_ItemTxt
	String_t* ___m_ItemTxt_2;
	// UnityEngine.UI.Text LangCtrl::m_Text
	Text_t1901882714 * ___m_Text_3;
	// System.String LangCtrl::tmpStr
	String_t* ___tmpStr_4;

public:
	inline static int32_t get_offset_of_m_ItemTxt_2() { return static_cast<int32_t>(offsetof(LangCtrl_t2672849805, ___m_ItemTxt_2)); }
	inline String_t* get_m_ItemTxt_2() const { return ___m_ItemTxt_2; }
	inline String_t** get_address_of_m_ItemTxt_2() { return &___m_ItemTxt_2; }
	inline void set_m_ItemTxt_2(String_t* value)
	{
		___m_ItemTxt_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemTxt_2), value);
	}

	inline static int32_t get_offset_of_m_Text_3() { return static_cast<int32_t>(offsetof(LangCtrl_t2672849805, ___m_Text_3)); }
	inline Text_t1901882714 * get_m_Text_3() const { return ___m_Text_3; }
	inline Text_t1901882714 ** get_address_of_m_Text_3() { return &___m_Text_3; }
	inline void set_m_Text_3(Text_t1901882714 * value)
	{
		___m_Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_3), value);
	}

	inline static int32_t get_offset_of_tmpStr_4() { return static_cast<int32_t>(offsetof(LangCtrl_t2672849805, ___tmpStr_4)); }
	inline String_t* get_tmpStr_4() const { return ___tmpStr_4; }
	inline String_t** get_address_of_tmpStr_4() { return &___tmpStr_4; }
	inline void set_tmpStr_4(String_t* value)
	{
		___tmpStr_4 = value;
		Il2CppCodeGenWriteBarrier((&___tmpStr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGCTRL_T2672849805_H
#ifndef CRGTSPRITETOGGLE_T4126742132_H
#define CRGTSPRITETOGGLE_T4126742132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTSpriteToggle
struct  CRGTSpriteToggle_t4126742132  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite CRGTSpriteToggle::ImageOn
	Sprite_t280657092 * ___ImageOn_2;
	// UnityEngine.Sprite CRGTSpriteToggle::ImageOff
	Sprite_t280657092 * ___ImageOff_3;
	// UnityEngine.Sprite CRGTSpriteToggle::ImageLocked
	Sprite_t280657092 * ___ImageLocked_4;
	// UnityEngine.UI.Image CRGTSpriteToggle::targetImage
	Image_t2670269651 * ___targetImage_5;
	// System.Int32 CRGTSpriteToggle::toggleState
	int32_t ___toggleState_6;

public:
	inline static int32_t get_offset_of_ImageOn_2() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___ImageOn_2)); }
	inline Sprite_t280657092 * get_ImageOn_2() const { return ___ImageOn_2; }
	inline Sprite_t280657092 ** get_address_of_ImageOn_2() { return &___ImageOn_2; }
	inline void set_ImageOn_2(Sprite_t280657092 * value)
	{
		___ImageOn_2 = value;
		Il2CppCodeGenWriteBarrier((&___ImageOn_2), value);
	}

	inline static int32_t get_offset_of_ImageOff_3() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___ImageOff_3)); }
	inline Sprite_t280657092 * get_ImageOff_3() const { return ___ImageOff_3; }
	inline Sprite_t280657092 ** get_address_of_ImageOff_3() { return &___ImageOff_3; }
	inline void set_ImageOff_3(Sprite_t280657092 * value)
	{
		___ImageOff_3 = value;
		Il2CppCodeGenWriteBarrier((&___ImageOff_3), value);
	}

	inline static int32_t get_offset_of_ImageLocked_4() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___ImageLocked_4)); }
	inline Sprite_t280657092 * get_ImageLocked_4() const { return ___ImageLocked_4; }
	inline Sprite_t280657092 ** get_address_of_ImageLocked_4() { return &___ImageLocked_4; }
	inline void set_ImageLocked_4(Sprite_t280657092 * value)
	{
		___ImageLocked_4 = value;
		Il2CppCodeGenWriteBarrier((&___ImageLocked_4), value);
	}

	inline static int32_t get_offset_of_targetImage_5() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___targetImage_5)); }
	inline Image_t2670269651 * get_targetImage_5() const { return ___targetImage_5; }
	inline Image_t2670269651 ** get_address_of_targetImage_5() { return &___targetImage_5; }
	inline void set_targetImage_5(Image_t2670269651 * value)
	{
		___targetImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetImage_5), value);
	}

	inline static int32_t get_offset_of_toggleState_6() { return static_cast<int32_t>(offsetof(CRGTSpriteToggle_t4126742132, ___toggleState_6)); }
	inline int32_t get_toggleState_6() const { return ___toggleState_6; }
	inline int32_t* get_address_of_toggleState_6() { return &___toggleState_6; }
	inline void set_toggleState_6(int32_t value)
	{
		___toggleState_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTSPRITETOGGLE_T4126742132_H
#ifndef CRGTREMOVEONTOUCH_T2211313123_H
#define CRGTREMOVEONTOUCH_T2211313123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTRemoveOnTouch
struct  CRGTRemoveOnTouch_t2211313123  : public MonoBehaviour_t3962482529
{
public:
	// System.String CRGTRemoveOnTouch::TargetTag
	String_t* ___TargetTag_2;
	// System.Boolean CRGTRemoveOnTouch::removeTarget
	bool ___removeTarget_3;
	// System.Single CRGTRemoveOnTouch::removeTargetTime
	float ___removeTargetTime_4;
	// System.Boolean CRGTRemoveOnTouch::removeSelf
	bool ___removeSelf_5;
	// System.Single CRGTRemoveOnTouch::removeSelfTime
	float ___removeSelfTime_6;

public:
	inline static int32_t get_offset_of_TargetTag_2() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___TargetTag_2)); }
	inline String_t* get_TargetTag_2() const { return ___TargetTag_2; }
	inline String_t** get_address_of_TargetTag_2() { return &___TargetTag_2; }
	inline void set_TargetTag_2(String_t* value)
	{
		___TargetTag_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTag_2), value);
	}

	inline static int32_t get_offset_of_removeTarget_3() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeTarget_3)); }
	inline bool get_removeTarget_3() const { return ___removeTarget_3; }
	inline bool* get_address_of_removeTarget_3() { return &___removeTarget_3; }
	inline void set_removeTarget_3(bool value)
	{
		___removeTarget_3 = value;
	}

	inline static int32_t get_offset_of_removeTargetTime_4() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeTargetTime_4)); }
	inline float get_removeTargetTime_4() const { return ___removeTargetTime_4; }
	inline float* get_address_of_removeTargetTime_4() { return &___removeTargetTime_4; }
	inline void set_removeTargetTime_4(float value)
	{
		___removeTargetTime_4 = value;
	}

	inline static int32_t get_offset_of_removeSelf_5() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeSelf_5)); }
	inline bool get_removeSelf_5() const { return ___removeSelf_5; }
	inline bool* get_address_of_removeSelf_5() { return &___removeSelf_5; }
	inline void set_removeSelf_5(bool value)
	{
		___removeSelf_5 = value;
	}

	inline static int32_t get_offset_of_removeSelfTime_6() { return static_cast<int32_t>(offsetof(CRGTRemoveOnTouch_t2211313123, ___removeSelfTime_6)); }
	inline float get_removeSelfTime_6() const { return ___removeSelfTime_6; }
	inline float* get_address_of_removeSelfTime_6() { return &___removeSelfTime_6; }
	inline void set_removeSelfTime_6(float value)
	{
		___removeSelfTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTREMOVEONTOUCH_T2211313123_H
#ifndef CRGTOBJECTMOVER_T1733178804_H
#define CRGTOBJECTMOVER_T1733178804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTObjectMover
struct  CRGTObjectMover_t1733178804  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CRGTObjectMover::objectSpeed
	float ___objectSpeed_2;
	// System.Single CRGTObjectMover::objectMoveX
	float ___objectMoveX_3;
	// System.Single CRGTObjectMover::objectMoveY
	float ___objectMoveY_4;
	// System.Single CRGTObjectMover::objectMoveZ
	float ___objectMoveZ_5;

public:
	inline static int32_t get_offset_of_objectSpeed_2() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectSpeed_2)); }
	inline float get_objectSpeed_2() const { return ___objectSpeed_2; }
	inline float* get_address_of_objectSpeed_2() { return &___objectSpeed_2; }
	inline void set_objectSpeed_2(float value)
	{
		___objectSpeed_2 = value;
	}

	inline static int32_t get_offset_of_objectMoveX_3() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectMoveX_3)); }
	inline float get_objectMoveX_3() const { return ___objectMoveX_3; }
	inline float* get_address_of_objectMoveX_3() { return &___objectMoveX_3; }
	inline void set_objectMoveX_3(float value)
	{
		___objectMoveX_3 = value;
	}

	inline static int32_t get_offset_of_objectMoveY_4() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectMoveY_4)); }
	inline float get_objectMoveY_4() const { return ___objectMoveY_4; }
	inline float* get_address_of_objectMoveY_4() { return &___objectMoveY_4; }
	inline void set_objectMoveY_4(float value)
	{
		___objectMoveY_4 = value;
	}

	inline static int32_t get_offset_of_objectMoveZ_5() { return static_cast<int32_t>(offsetof(CRGTObjectMover_t1733178804, ___objectMoveZ_5)); }
	inline float get_objectMoveZ_5() const { return ___objectMoveZ_5; }
	inline float* get_address_of_objectMoveZ_5() { return &___objectMoveZ_5; }
	inline void set_objectMoveZ_5(float value)
	{
		___objectMoveZ_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTOBJECTMOVER_T1733178804_H
#ifndef CRGTPLAYERCONTROLLER_T3312304059_H
#define CRGTPLAYERCONTROLLER_T3312304059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRGTPlayerController
struct  CRGTPlayerController_t3312304059  : public MonoBehaviour_t3962482529
{
public:
	// ControlType CRGTPlayerController::playerControlType
	int32_t ___playerControlType_2;
	// System.Single CRGTPlayerController::playerSideSpeed
	float ___playerSideSpeed_5;
	// System.Boolean CRGTPlayerController::movePlayerRot
	bool ___movePlayerRot_6;
	// System.Single CRGTPlayerController::playerRot
	float ___playerRot_7;
	// System.Single CRGTPlayerController::playerRotSpeed
	float ___playerRotSpeed_8;
	// System.Single[] CRGTPlayerController::spawnPlayerXPos
	SingleU5BU5D_t1444911251* ___spawnPlayerXPos_9;
	// System.Single CRGTPlayerController::playerMinX
	float ___playerMinX_10;
	// System.Single CRGTPlayerController::playerMaxX
	float ___playerMaxX_11;
	// UnityEngine.GameObject CRGTPlayerController::crashParticles
	GameObject_t1113636619 * ___crashParticles_12;
	// UnityEngine.GameObject CRGTPlayerController::turboEffect
	GameObject_t1113636619 * ___turboEffect_13;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboFlareL
	ParticleSystem_t1800779281 * ___turboFlareL_14;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboFlareR
	ParticleSystem_t1800779281 * ___turboFlareR_15;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboCoreL
	ParticleSystem_t1800779281 * ___turboCoreL_16;
	// UnityEngine.ParticleSystem CRGTPlayerController::turboCoreR
	ParticleSystem_t1800779281 * ___turboCoreR_17;
	// System.Single CRGTPlayerController::rotationMultiplier
	float ___rotationMultiplier_18;
	// UnityEngine.AudioClip CRGTPlayerController::carCrash
	AudioClip_t3680889665 * ___carCrash_19;
	// System.Single CRGTPlayerController::playerPosY
	float ___playerPosY_20;
	// System.Single CRGTPlayerController::playerVelX
	float ___playerVelX_21;
	// UnityEngine.Rigidbody2D CRGTPlayerController::rigBody2D
	Rigidbody2D_t939494601 * ___rigBody2D_22;

public:
	inline static int32_t get_offset_of_playerControlType_2() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerControlType_2)); }
	inline int32_t get_playerControlType_2() const { return ___playerControlType_2; }
	inline int32_t* get_address_of_playerControlType_2() { return &___playerControlType_2; }
	inline void set_playerControlType_2(int32_t value)
	{
		___playerControlType_2 = value;
	}

	inline static int32_t get_offset_of_playerSideSpeed_5() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerSideSpeed_5)); }
	inline float get_playerSideSpeed_5() const { return ___playerSideSpeed_5; }
	inline float* get_address_of_playerSideSpeed_5() { return &___playerSideSpeed_5; }
	inline void set_playerSideSpeed_5(float value)
	{
		___playerSideSpeed_5 = value;
	}

	inline static int32_t get_offset_of_movePlayerRot_6() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___movePlayerRot_6)); }
	inline bool get_movePlayerRot_6() const { return ___movePlayerRot_6; }
	inline bool* get_address_of_movePlayerRot_6() { return &___movePlayerRot_6; }
	inline void set_movePlayerRot_6(bool value)
	{
		___movePlayerRot_6 = value;
	}

	inline static int32_t get_offset_of_playerRot_7() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerRot_7)); }
	inline float get_playerRot_7() const { return ___playerRot_7; }
	inline float* get_address_of_playerRot_7() { return &___playerRot_7; }
	inline void set_playerRot_7(float value)
	{
		___playerRot_7 = value;
	}

	inline static int32_t get_offset_of_playerRotSpeed_8() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerRotSpeed_8)); }
	inline float get_playerRotSpeed_8() const { return ___playerRotSpeed_8; }
	inline float* get_address_of_playerRotSpeed_8() { return &___playerRotSpeed_8; }
	inline void set_playerRotSpeed_8(float value)
	{
		___playerRotSpeed_8 = value;
	}

	inline static int32_t get_offset_of_spawnPlayerXPos_9() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___spawnPlayerXPos_9)); }
	inline SingleU5BU5D_t1444911251* get_spawnPlayerXPos_9() const { return ___spawnPlayerXPos_9; }
	inline SingleU5BU5D_t1444911251** get_address_of_spawnPlayerXPos_9() { return &___spawnPlayerXPos_9; }
	inline void set_spawnPlayerXPos_9(SingleU5BU5D_t1444911251* value)
	{
		___spawnPlayerXPos_9 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPlayerXPos_9), value);
	}

	inline static int32_t get_offset_of_playerMinX_10() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerMinX_10)); }
	inline float get_playerMinX_10() const { return ___playerMinX_10; }
	inline float* get_address_of_playerMinX_10() { return &___playerMinX_10; }
	inline void set_playerMinX_10(float value)
	{
		___playerMinX_10 = value;
	}

	inline static int32_t get_offset_of_playerMaxX_11() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerMaxX_11)); }
	inline float get_playerMaxX_11() const { return ___playerMaxX_11; }
	inline float* get_address_of_playerMaxX_11() { return &___playerMaxX_11; }
	inline void set_playerMaxX_11(float value)
	{
		___playerMaxX_11 = value;
	}

	inline static int32_t get_offset_of_crashParticles_12() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___crashParticles_12)); }
	inline GameObject_t1113636619 * get_crashParticles_12() const { return ___crashParticles_12; }
	inline GameObject_t1113636619 ** get_address_of_crashParticles_12() { return &___crashParticles_12; }
	inline void set_crashParticles_12(GameObject_t1113636619 * value)
	{
		___crashParticles_12 = value;
		Il2CppCodeGenWriteBarrier((&___crashParticles_12), value);
	}

	inline static int32_t get_offset_of_turboEffect_13() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboEffect_13)); }
	inline GameObject_t1113636619 * get_turboEffect_13() const { return ___turboEffect_13; }
	inline GameObject_t1113636619 ** get_address_of_turboEffect_13() { return &___turboEffect_13; }
	inline void set_turboEffect_13(GameObject_t1113636619 * value)
	{
		___turboEffect_13 = value;
		Il2CppCodeGenWriteBarrier((&___turboEffect_13), value);
	}

	inline static int32_t get_offset_of_turboFlareL_14() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboFlareL_14)); }
	inline ParticleSystem_t1800779281 * get_turboFlareL_14() const { return ___turboFlareL_14; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboFlareL_14() { return &___turboFlareL_14; }
	inline void set_turboFlareL_14(ParticleSystem_t1800779281 * value)
	{
		___turboFlareL_14 = value;
		Il2CppCodeGenWriteBarrier((&___turboFlareL_14), value);
	}

	inline static int32_t get_offset_of_turboFlareR_15() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboFlareR_15)); }
	inline ParticleSystem_t1800779281 * get_turboFlareR_15() const { return ___turboFlareR_15; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboFlareR_15() { return &___turboFlareR_15; }
	inline void set_turboFlareR_15(ParticleSystem_t1800779281 * value)
	{
		___turboFlareR_15 = value;
		Il2CppCodeGenWriteBarrier((&___turboFlareR_15), value);
	}

	inline static int32_t get_offset_of_turboCoreL_16() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboCoreL_16)); }
	inline ParticleSystem_t1800779281 * get_turboCoreL_16() const { return ___turboCoreL_16; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboCoreL_16() { return &___turboCoreL_16; }
	inline void set_turboCoreL_16(ParticleSystem_t1800779281 * value)
	{
		___turboCoreL_16 = value;
		Il2CppCodeGenWriteBarrier((&___turboCoreL_16), value);
	}

	inline static int32_t get_offset_of_turboCoreR_17() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___turboCoreR_17)); }
	inline ParticleSystem_t1800779281 * get_turboCoreR_17() const { return ___turboCoreR_17; }
	inline ParticleSystem_t1800779281 ** get_address_of_turboCoreR_17() { return &___turboCoreR_17; }
	inline void set_turboCoreR_17(ParticleSystem_t1800779281 * value)
	{
		___turboCoreR_17 = value;
		Il2CppCodeGenWriteBarrier((&___turboCoreR_17), value);
	}

	inline static int32_t get_offset_of_rotationMultiplier_18() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___rotationMultiplier_18)); }
	inline float get_rotationMultiplier_18() const { return ___rotationMultiplier_18; }
	inline float* get_address_of_rotationMultiplier_18() { return &___rotationMultiplier_18; }
	inline void set_rotationMultiplier_18(float value)
	{
		___rotationMultiplier_18 = value;
	}

	inline static int32_t get_offset_of_carCrash_19() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___carCrash_19)); }
	inline AudioClip_t3680889665 * get_carCrash_19() const { return ___carCrash_19; }
	inline AudioClip_t3680889665 ** get_address_of_carCrash_19() { return &___carCrash_19; }
	inline void set_carCrash_19(AudioClip_t3680889665 * value)
	{
		___carCrash_19 = value;
		Il2CppCodeGenWriteBarrier((&___carCrash_19), value);
	}

	inline static int32_t get_offset_of_playerPosY_20() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerPosY_20)); }
	inline float get_playerPosY_20() const { return ___playerPosY_20; }
	inline float* get_address_of_playerPosY_20() { return &___playerPosY_20; }
	inline void set_playerPosY_20(float value)
	{
		___playerPosY_20 = value;
	}

	inline static int32_t get_offset_of_playerVelX_21() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___playerVelX_21)); }
	inline float get_playerVelX_21() const { return ___playerVelX_21; }
	inline float* get_address_of_playerVelX_21() { return &___playerVelX_21; }
	inline void set_playerVelX_21(float value)
	{
		___playerVelX_21 = value;
	}

	inline static int32_t get_offset_of_rigBody2D_22() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059, ___rigBody2D_22)); }
	inline Rigidbody2D_t939494601 * get_rigBody2D_22() const { return ___rigBody2D_22; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigBody2D_22() { return &___rigBody2D_22; }
	inline void set_rigBody2D_22(Rigidbody2D_t939494601 * value)
	{
		___rigBody2D_22 = value;
		Il2CppCodeGenWriteBarrier((&___rigBody2D_22), value);
	}
};

struct CRGTPlayerController_t3312304059_StaticFields
{
public:
	// System.Boolean CRGTPlayerController::isAlive
	bool ___isAlive_3;
	// System.Single CRGTPlayerController::playerSpeed
	float ___playerSpeed_4;

public:
	inline static int32_t get_offset_of_isAlive_3() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059_StaticFields, ___isAlive_3)); }
	inline bool get_isAlive_3() const { return ___isAlive_3; }
	inline bool* get_address_of_isAlive_3() { return &___isAlive_3; }
	inline void set_isAlive_3(bool value)
	{
		___isAlive_3 = value;
	}

	inline static int32_t get_offset_of_playerSpeed_4() { return static_cast<int32_t>(offsetof(CRGTPlayerController_t3312304059_StaticFields, ___playerSpeed_4)); }
	inline float get_playerSpeed_4() const { return ___playerSpeed_4; }
	inline float* get_address_of_playerSpeed_4() { return &___playerSpeed_4; }
	inline void set_playerSpeed_4(float value)
	{
		___playerSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRGTPLAYERCONTROLLER_T3312304059_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
// System.Single[]
struct SingleU5BU5D_t1444911251  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CRGTSpriteToggle[]
struct CRGTSpriteToggleU5BU5D_t2845023549  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CRGTSpriteToggle_t4126742132 * m_Items[1];

public:
	inline CRGTSpriteToggle_t4126742132 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CRGTSpriteToggle_t4126742132 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CRGTSpriteToggle_t4126742132 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CRGTSpriteToggle_t4126742132 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CRGTSpriteToggle_t4126742132 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CRGTSpriteToggle_t4126742132 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  RuntimeObject * Resources_Load_TisRuntimeObject_m597869152_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m2716693327 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::UpdateScore(System.Int32)
extern "C"  void CRGTGameManager_UpdateScore_m1298736932 (CRGTGameManager_t3284010685 * __this, int32_t ___scoreValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTSoundManager::PlaySound(UnityEngine.AudioClip)
extern "C"  void CRGTSoundManager_PlaySound_m636038493 (CRGTSoundManager_t434062966 * __this, AudioClip_t3680889665 * ___clip0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1113636619_m3006960551(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m3118546832 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Transform>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisTransform_t3600365921_m1831857851(__this /* static, unused */, p0, p1, p2, method) ((  Transform_t3600365921 * (*) (RuntimeObject * /* static, unused */, Transform_t3600365921 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t1901882714_m4196288697(__this, method) ((  Text_t1901882714 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.CanvasGroup>()
#define Component_GetComponentInChildren_TisCanvasGroup_t4083511760_m2486821986(__this, method) ((  CanvasGroup_t4083511760 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C"  void CanvasGroup_set_alpha_m4032573 (CanvasGroup_t4083511760 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CRGTFader::StartFade(System.Boolean)
extern "C"  RuntimeObject* CRGTFader_StartFade_m4220097985 (CRGTFader_t912498268 * __this, bool ___fade0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTFader/<StartFade>c__Iterator0::.ctor()
extern "C"  void U3CStartFadeU3Ec__Iterator0__ctor_m2382299632 (U3CStartFadeU3Ec__Iterator0_t3292164234 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
#define Component_GetComponent_TisGUIText_t402233326_m4001847390(__this, method) ((  GUIText_t402233326 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Time::get_timeScale()
extern "C"  float Time_get_timeScale_m701790074 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C"  void GUIText_set_text_m2265981083 (GUIText_t402233326 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t2555686324  Color_get_yellow_m1287957903 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_color(UnityEngine.Color)
extern "C"  void GUIText_set_color_m596217133 (GUIText_t402233326 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2555686324  Color_get_red_m3227813939 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2555686324  Color_get_green_m490390750 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3117905507 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t1871169219  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C"  void Time_set_timeScale_m1127545364 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C"  void Screen_set_sleepTimeout_m2277210665 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::LoadGameData()
extern "C"  void CRGTGameManager_LoadGameData_m380892872 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::ShowGameMenu()
extern "C"  void CRGTGameManager_ShowGameMenu_m2205210472 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::SpawnNewObject()
extern "C"  void CRGTGameManager_SpawnNewObject_m3505301815 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::UpdateGameData()
extern "C"  void CRGTGameManager_UpdateGameData_m3678386969 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m381167889 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m253439912 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m3145433196 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m622963475 (Scene_t2348375561 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m1299643124 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::DisplayBanner(System.Boolean)
extern "C"  void CRGTGameManager_DisplayBanner_m3605215792 (CRGTGameManager_t3284010685 * __this, bool ___show0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::ButtonSound()
extern "C"  void CRGTGameManager_ButtonSound_m513884379 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::GameResume()
extern "C"  void CRGTGameManager_GameResume_m2309066240 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::RequestInterstial()
extern "C"  void CRGTGameManager_RequestInterstial_m2120773455 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::DisplayInterstial()
extern "C"  void CRGTGameManager_DisplayInterstial_m49774147 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::CleanUpScene()
extern "C"  void CRGTGameManager_CleanUpScene_m4197824408 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::ShowGamePlayMenu()
extern "C"  void CRGTGameManager_ShowGamePlayMenu_m4164765505 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::GameStart()
extern "C"  void CRGTGameManager_GameStart_m2936842800 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::ShowPauseMenu()
extern "C"  void CRGTGameManager_ShowPauseMenu_m2882739331 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::GameOver()
extern "C"  void CRGTGameManager_GameOver_m3937302641 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::SaveGameData()
extern "C"  void CRGTGameManager_SaveGameData_m1501793627 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::ShowGameOverMenu()
extern "C"  void CRGTGameManager_ShowGameOverMenu_m1957943842 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m470877999 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTGameManager::GameQuitNow()
extern "C"  void CRGTGameManager_GameQuitNow_m3158605986 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, method) ((  Renderer_t2627027031 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m4171603682 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_mainTextureOffset(UnityEngine.Vector2)
extern "C"  void Material_set_mainTextureOffset_m2452486895 (Material_t340375123 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m1810197270 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, method) ((  Rigidbody2D_t939494601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern "C"  Vector2_t2156229523  Rigidbody2D_get_position_m2575647076 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_position(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_position_m1659195711 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_acceleration()
extern "C"  Vector3_t3722313464  Input_get_acceleration_m2528400370 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m214549210 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2156229523  Touch_get_position_m3109777936 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m2898400508 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t2156229523  Rigidbody2D_get_velocity_m366589732 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_rotation()
extern "C"  float Rigidbody2D_get_rotation_m1584227507 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1004423579 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_rotation(System.Single)
extern "C"  void Rigidbody2D_set_rotation_m1493600238 (Rigidbody2D_t939494601 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTSoundManager::UpdateVolume()
extern "C"  void CRGTSoundManager_UpdateVolume_m617425764 (CRGTSoundManager_t434062966 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
extern "C"  float PlayerPrefs_GetFloat_m3293813615 (RuntimeObject * __this /* static, unused */, String_t* p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C"  void AudioSource_set_volume_m1273312851 (AudioSource_t3935305588 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Convert::ToBoolean(System.Int32)
extern "C"  bool Convert_ToBoolean_m2833489984 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTSoundManager::SetSoundOnOff(System.Boolean)
extern "C"  void CRGTSoundManager_SetSoundOnOff_m376341164 (CRGTSoundManager_t434062966 * __this, bool ___off0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m31653938 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m48294159 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern "C"  void AudioSource_set_mute_m3553377094 (AudioSource_t3935305588 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Convert::ToInt32(System.Boolean)
extern "C"  int32_t Convert_ToInt32_m2100527582 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CRGTSpriteToggle::SetToggleValue(System.Int32)
extern "C"  void CRGTSpriteToggle_SetToggleValue_m212693942 (CRGTSpriteToggle_t4126742132 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSource::get_mute()
extern "C"  bool AudioSource_get_mute_m2424389661 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LangCtrl::SetLang()
extern "C"  void LangCtrl_SetLang_m691616426 (LangCtrl_t2672849805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LangCtrl::CheckLangType()
extern "C"  void LangCtrl_CheckLangType_m87832360 (LangCtrl_t2672849805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Resources::Load<UnityEngine.TextAsset>(System.String)
#define Resources_Load_TisTextAsset_t3022178571_m724759995(__this /* static, unused */, p0, method) ((  TextAsset_t3022178571 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m597869152_gshared)(__this /* static, unused */, p0, method)
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m2027878391 (TextAsset_t3022178571 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonMapper::ToObject(System.String)
extern "C"  JsonData_t1524858407 * JsonMapper_ToObject_m190320911 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::get_Item(System.String)
extern "C"  JsonData_t1524858407 * JsonData_get_Item_m1449041990 (JsonData_t1524858407 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern "C"  void PlayerPrefs_SetString_m2101271233 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
extern "C"  int32_t Application_get_systemLanguage_m3110370732 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTBonus::.ctor()
extern "C"  void CRGTBonus__ctor_m1143685059 (CRGTBonus_t461169207 * __this, const RuntimeMethod* method)
{
	{
		__this->set_scoreValue_4(((int32_t)10));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTBonus::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void CRGTBonus_OnTriggerEnter2D_m1670428559 (CRGTBonus_t461169207 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTBonus_OnTriggerEnter2D_m1670428559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1113636619 * V_3 = NULL;
	Transform_t3600365921 * V_4 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00fd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_3 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		int32_t L_4 = __this->get_scoreValue_4();
		NullCheck(L_3);
		CRGTGameManager_UpdateScore_m1298736932(L_3, L_4, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_5 = __this->get_soundBonus_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var);
		CRGTSoundManager_t434062966 * L_7 = ((CRGTSoundManager_t434062966_StaticFields*)il2cpp_codegen_static_fields_for(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var))->get_instance_2();
		AudioClip_t3680889665 * L_8 = __this->get_soundBonus_5();
		NullCheck(L_7);
		CRGTSoundManager_PlaySound_m636038493(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0045:
	{
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_x_1();
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		float L_14 = (&V_2)->get_y_2();
		Vector3__ctor_m3353183577((&V_0), L_11, L_14, (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_15 = __this->get_bonusParticles_3();
		Vector3_t3722313464  L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_17 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_18 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_3 = L_18;
		GameObject_t1113636619 * L_19 = V_3;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_19, (1.0f), /*hidden argument*/NULL);
		Transform_t3600365921 * L_20 = __this->get_scoreEffect_2();
		bool L_21 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00f2;
		}
	}
	{
		Transform_t3600365921 * L_22 = __this->get_scoreEffect_2();
		Transform_t3600365921 * L_23 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t3722313464  L_24 = Transform_get_position_m36019626(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_25 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Transform_t3600365921 * L_26 = Object_Instantiate_TisTransform_t3600365921_m1831857851(NULL /*static, unused*/, L_22, L_24, L_25, /*hidden argument*/Object_Instantiate_TisTransform_t3600365921_m1831857851_RuntimeMethod_var);
		V_4 = L_26;
		Transform_t3600365921 * L_27 = V_4;
		NullCheck(L_27);
		Transform_t3600365921 * L_28 = Transform_Find_m1729760951(L_27, _stringLiteral3987835886, /*hidden argument*/NULL);
		NullCheck(L_28);
		Text_t1901882714 * L_29 = Component_GetComponent_TisText_t1901882714_m4196288697(L_28, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		int32_t* L_30 = __this->get_address_of_scoreValue_4();
		String_t* L_31 = Int32_ToString_m141394615(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3452614533, L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_29, L_32);
	}

IL_00f2:
	{
		GameObject_t1113636619 * L_33 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTFader::.ctor()
extern "C"  void CRGTFader__ctor_m4241730098 (CRGTFader_t912498268 * __this, const RuntimeMethod* method)
{
	{
		__this->set_fadeTime_3((0.25f));
		__this->set_fadeFinal_4((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTFader::Start()
extern "C"  void CRGTFader_Start_m864434492 (CRGTFader_t912498268 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTFader_Start_m864434492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CanvasGroup_t4083511760 * L_0 = Component_GetComponentInChildren_TisCanvasGroup_t4083511760_m2486821986(__this, /*hidden argument*/Component_GetComponentInChildren_TisCanvasGroup_t4083511760_m2486821986_RuntimeMethod_var);
		__this->set_faderGroup_2(L_0);
		return;
	}
}
// System.Void CRGTFader::OnEnable()
extern "C"  void CRGTFader_OnEnable_m3391193346 (CRGTFader_t912498268 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTFader_OnEnable_m3391193346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CanvasGroup_t4083511760 * L_0 = __this->get_faderGroup_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		CanvasGroup_t4083511760 * L_2 = Component_GetComponentInChildren_TisCanvasGroup_t4083511760_m2486821986(__this, /*hidden argument*/Component_GetComponentInChildren_TisCanvasGroup_t4083511760_m2486821986_RuntimeMethod_var);
		__this->set_faderGroup_2(L_2);
	}

IL_001d:
	{
		CanvasGroup_t4083511760 * L_3 = __this->get_faderGroup_2();
		NullCheck(L_3);
		CanvasGroup_set_alpha_m4032573(L_3, (0.0f), /*hidden argument*/NULL);
		RuntimeObject* L_4 = CRGTFader_StartFade_m4220097985(__this, (bool)1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CRGTFader::StartFade(System.Boolean)
extern "C"  RuntimeObject* CRGTFader_StartFade_m4220097985 (CRGTFader_t912498268 * __this, bool ___fade0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTFader_StartFade_m4220097985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartFadeU3Ec__Iterator0_t3292164234 * V_0 = NULL;
	{
		U3CStartFadeU3Ec__Iterator0_t3292164234 * L_0 = (U3CStartFadeU3Ec__Iterator0_t3292164234 *)il2cpp_codegen_object_new(U3CStartFadeU3Ec__Iterator0_t3292164234_il2cpp_TypeInfo_var);
		U3CStartFadeU3Ec__Iterator0__ctor_m2382299632(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartFadeU3Ec__Iterator0_t3292164234 * L_1 = V_0;
		bool L_2 = ___fade0;
		NullCheck(L_1);
		L_1->set_fade_2(L_2);
		U3CStartFadeU3Ec__Iterator0_t3292164234 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_3(__this);
		U3CStartFadeU3Ec__Iterator0_t3292164234 * L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTFader/<StartFade>c__Iterator0::.ctor()
extern "C"  void U3CStartFadeU3Ec__Iterator0__ctor_m2382299632 (U3CStartFadeU3Ec__Iterator0_t3292164234 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CRGTFader/<StartFade>c__Iterator0::MoveNext()
extern "C"  bool U3CStartFadeU3Ec__Iterator0_MoveNext_m620204448 (U3CStartFadeU3Ec__Iterator0_t3292164234 * __this, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_005c;
			}
			case 2:
			{
				goto IL_00ea;
			}
		}
	}
	{
		goto IL_0102;
	}

IL_0025:
	{
		__this->set_U3CelapsedTimeU3E__0_0((0.0f));
		CRGTFader_t912498268 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		float L_3 = L_2->get_fadeTime_3();
		__this->set_U3CwaitU3E__0_1(L_3);
		__this->set_U24current_4(NULL);
		bool L_4 = __this->get_U24disposing_5();
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0057:
	{
		goto IL_0104;
	}

IL_005c:
	{
		goto IL_00ea;
	}

IL_0061:
	{
		bool L_5 = __this->get_fade_2();
		if (!L_5)
		{
			goto IL_009a;
		}
	}
	{
		CRGTFader_t912498268 * L_6 = __this->get_U24this_3();
		NullCheck(L_6);
		CanvasGroup_t4083511760 * L_7 = L_6->get_faderGroup_2();
		float L_8 = __this->get_U3CelapsedTimeU3E__0_0();
		float L_9 = __this->get_U3CwaitU3E__0_1();
		CRGTFader_t912498268 * L_10 = __this->get_U24this_3();
		NullCheck(L_10);
		float L_11 = L_10->get_fadeFinal_4();
		NullCheck(L_7);
		CanvasGroup_set_alpha_m4032573(L_7, ((float)il2cpp_codegen_multiply((float)((float)((float)L_8/(float)L_9)), (float)L_11)), /*hidden argument*/NULL);
		goto IL_00bd;
	}

IL_009a:
	{
		CRGTFader_t912498268 * L_12 = __this->get_U24this_3();
		NullCheck(L_12);
		CanvasGroup_t4083511760 * L_13 = L_12->get_faderGroup_2();
		float L_14 = __this->get_U3CelapsedTimeU3E__0_0();
		float L_15 = __this->get_U3CwaitU3E__0_1();
		NullCheck(L_13);
		CanvasGroup_set_alpha_m4032573(L_13, ((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)((float)L_14/(float)L_15)))), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		float L_16 = __this->get_U3CelapsedTimeU3E__0_0();
		float L_17 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CelapsedTimeU3E__0_0(((float)il2cpp_codegen_add((float)L_16, (float)L_17)));
		__this->set_U24current_4(NULL);
		bool L_18 = __this->get_U24disposing_5();
		if (L_18)
		{
			goto IL_00e5;
		}
	}
	{
		__this->set_U24PC_6(2);
	}

IL_00e5:
	{
		goto IL_0104;
	}

IL_00ea:
	{
		float L_19 = __this->get_U3CelapsedTimeU3E__0_0();
		float L_20 = __this->get_U3CwaitU3E__0_1();
		if ((((float)L_19) < ((float)L_20)))
		{
			goto IL_0061;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_0102:
	{
		return (bool)0;
	}

IL_0104:
	{
		return (bool)1;
	}
}
// System.Object CRGTFader/<StartFade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CStartFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m382051057 (U3CStartFadeU3Ec__Iterator0_t3292164234 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object CRGTFader/<StartFade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CStartFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3718221093 (U3CStartFadeU3Ec__Iterator0_t3292164234 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void CRGTFader/<StartFade>c__Iterator0::Dispose()
extern "C"  void U3CStartFadeU3Ec__Iterator0_Dispose_m1924255382 (U3CStartFadeU3Ec__Iterator0_t3292164234 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void CRGTFader/<StartFade>c__Iterator0::Reset()
extern "C"  void U3CStartFadeU3Ec__Iterator0_Reset_m3269543568 (U3CStartFadeU3Ec__Iterator0_t3292164234 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartFadeU3Ec__Iterator0_Reset_m3269543568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CStartFadeU3Ec__Iterator0_Reset_m3269543568_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTFps::.ctor()
extern "C"  void CRGTFps__ctor_m4218976117 (CRGTFps_t1814840959 * __this, const RuntimeMethod* method)
{
	{
		__this->set_updateInterval_2((0.5f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTFps::Start()
extern "C"  void CRGTFps_Start_m3597135823 (CRGTFps_t1814840959 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTFps_Start_m3597135823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_updateInterval_2();
		__this->set_timeLeft_5(L_0);
		GUIText_t402233326 * L_1 = Component_GetComponent_TisGUIText_t402233326_m4001847390(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t402233326_m4001847390_RuntimeMethod_var);
		__this->set_FpsText_6(L_1);
		return;
	}
}
// System.Void CRGTFps::Update()
extern "C"  void CRGTFps_Update_m3736617682 (CRGTFps_t1814840959 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTFps_Update_m3736617682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	String_t* V_1 = NULL;
	{
		float L_0 = __this->get_timeLeft_5();
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeLeft_5(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		float L_2 = __this->get_accum_3();
		float L_3 = Time_get_timeScale_m701790074(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_accum_3(((float)il2cpp_codegen_add((float)L_2, (float)((float)((float)L_3/(float)L_4)))));
		int32_t L_5 = __this->get_frames_4();
		__this->set_frames_4(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		float L_6 = __this->get_timeLeft_5();
		if ((!(((double)(((double)((double)L_6)))) <= ((double)(0.0)))))
		{
			goto IL_00e7;
		}
	}
	{
		float L_7 = __this->get_accum_3();
		int32_t L_8 = __this->get_frames_4();
		V_0 = ((float)((float)L_7/(float)(((float)((float)L_8)))));
		float L_9 = V_0;
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral4010238652, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		GUIText_t402233326 * L_13 = __this->get_FpsText_6();
		String_t* L_14 = V_1;
		NullCheck(L_13);
		GUIText_set_text_m2265981083(L_13, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		if ((!(((float)L_15) < ((float)(30.0f)))))
		{
			goto IL_0099;
		}
	}
	{
		GUIText_t402233326 * L_16 = __this->get_FpsText_6();
		Color_t2555686324  L_17 = Color_get_yellow_m1287957903(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIText_set_color_m596217133(L_16, L_17, /*hidden argument*/NULL);
		goto IL_00c9;
	}

IL_0099:
	{
		float L_18 = V_0;
		if ((!(((float)L_18) < ((float)(10.0f)))))
		{
			goto IL_00b9;
		}
	}
	{
		GUIText_t402233326 * L_19 = __this->get_FpsText_6();
		Color_t2555686324  L_20 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		GUIText_set_color_m596217133(L_19, L_20, /*hidden argument*/NULL);
		goto IL_00c9;
	}

IL_00b9:
	{
		GUIText_t402233326 * L_21 = __this->get_FpsText_6();
		Color_t2555686324  L_22 = Color_get_green_m490390750(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		GUIText_set_color_m596217133(L_21, L_22, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		float L_23 = __this->get_updateInterval_2();
		__this->set_timeLeft_5(L_23);
		__this->set_accum_3((0.0f));
		__this->set_frames_4(0);
	}

IL_00e7:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTGameManager::.ctor()
extern "C"  void CRGTGameManager__ctor_m2891170590 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager__ctor_m2891170590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_startGameSpeed_3((1.0f));
		__this->set_gameSpeed_4((1.0f));
		__this->set_spawnTime_9((2.0f));
		__this->set_spawnSpeed_10((2.0f));
		__this->set_isGameOver_11((bool)1);
		__this->set_startSpawnSpeed_14((2.0f));
		__this->set_spawnStep_15((0.05f));
		__this->set_minSpawSpeed_16((0.48f));
		SingleU5BU5D_t1444911251* L_0 = ((SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeFieldHandle_t1871169219  L_1 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255366____U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, L_1, /*hidden argument*/NULL);
		__this->set_spawnObjectsXPos_18(L_0);
		__this->set_gameOverURL_34(_stringLiteral400424176);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::Awake()
extern "C"  void CRGTGameManager_Awake_m2006082390 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager_Awake_m2006082390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_0 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->set_instance_2(__this);
		goto IL_0036;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_2 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void CRGTGameManager::Start()
extern "C"  void CRGTGameManager_Start_m1028551180 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_startGameSpeed_3();
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Screen_set_sleepTimeout_m2277210665(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		CRGTGameManager_LoadGameData_m380892872(__this, /*hidden argument*/NULL);
		CRGTGameManager_ShowGameMenu_m2205210472(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::Update()
extern "C"  void CRGTGameManager_Update_m3609336709 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isGameOver_11();
		if (L_0)
		{
			goto IL_0099;
		}
	}
	{
		bool L_1 = __this->get_isGamePaused_12();
		if (L_1)
		{
			goto IL_0099;
		}
	}
	{
		float L_2 = __this->get_spawnTime_9();
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_spawnTime_9(((float)il2cpp_codegen_subtract((float)L_2, (float)L_3)));
		float L_4 = __this->get_spawnTime_9();
		if ((!(((float)L_4) <= ((float)(0.0f)))))
		{
			goto IL_0099;
		}
	}
	{
		CRGTGameManager_SpawnNewObject_m3505301815(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_spawnSpeed_10();
		float L_6 = __this->get_minSpawSpeed_16();
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = __this->get_spawnSpeed_10();
		float L_8 = __this->get_spawnStep_15();
		__this->set_spawnSpeed_10(((float)il2cpp_codegen_subtract((float)L_7, (float)L_8)));
		goto IL_0073;
	}

IL_0067:
	{
		float L_9 = __this->get_minSpawSpeed_16();
		__this->set_spawnSpeed_10(L_9);
	}

IL_0073:
	{
		float L_10 = __this->get_spawnSpeed_10();
		__this->set_spawnTime_9(L_10);
		int32_t L_11 = __this->get_gameScore_13();
		float L_12 = __this->get_gameSpeed_4();
		__this->set_gameScore_13(((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)(((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)(1.0f), (float)L_12))))))));
	}

IL_0099:
	{
		CRGTGameManager_UpdateGameData_m3678386969(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::SpawnNewObject()
extern "C"  void CRGTGameManager_SpawnNewObject_m3505301815 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager_SpawnNewObject_m3505301815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1113636619 * V_3 = NULL;
	{
		SingleU5BU5D_t1444911251* L_0 = __this->get_spawnObjectsXPos_18();
		SingleU5BU5D_t1444911251* L_1 = __this->get_spawnObjectsXPos_18();
		NullCheck(L_1);
		int32_t L_2 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = L_2;
		float L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		float L_5 = V_0;
		RectTransform_t3704657025 * L_6 = __this->get_spawnLine_17();
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		Vector3__ctor_m3353183577((&V_1), L_5, L_8, (0.0f), /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_9 = __this->get_spawnGameObjects_19();
		GameObjectU5BU5D_t3328599146* L_10 = __this->get_spawnGameObjects_19();
		NullCheck(L_10);
		int32_t L_11 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_12 = L_11;
		GameObject_t1113636619 * L_13 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		__this->set_spawnObject_20(L_13);
		GameObject_t1113636619 * L_14 = __this->get_spawnObject_20();
		Vector3_t3722313464  L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_16 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_17 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_3 = L_17;
		GameObject_t1113636619 * L_18 = V_3;
		NullCheck(L_18);
		Transform_t3600365921 * L_19 = GameObject_get_transform_m1369836730(L_18, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_20 = __this->get_spawnLine_17();
		NullCheck(L_19);
		Transform_SetParent_m381167889(L_19, L_20, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_21 = V_3;
		NullCheck(L_21);
		Transform_t3600365921 * L_22 = GameObject_get_transform_m1369836730(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_SetAsFirstSibling_m253439912(L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::CleanUpScene()
extern "C"  void CRGTGameManager_CleanUpScene_m4197824408 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager_CleanUpScene_m4197824408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		RectTransform_t3704657025 * L_0 = __this->get_spawnLine_17();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Transform_t3600365921 * L_2 = Transform_GetChild_m1092972975(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_5 = V_0;
		RectTransform_t3704657025 * L_6 = __this->get_spawnLine_17();
		NullCheck(L_6);
		int32_t L_7 = Transform_get_childCount_m3145433196(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CRGTGameManager::LoadGameData()
extern "C"  void CRGTGameManager_LoadGameData_m380892872 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager_LoadGameData_m380892872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t2348375561  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Scene_t2348375561  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m622963475((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, L_1, _stringLiteral16531513, /*hidden argument*/NULL);
		int32_t L_3 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, L_2, 0, /*hidden argument*/NULL);
		__this->set_highGameScore_5(L_3);
		Scene_t2348375561  L_4 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = Scene_get_name_m622963475((&V_1), /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, L_5, _stringLiteral4048657555, /*hidden argument*/NULL);
		int32_t L_7 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, L_6, 0, /*hidden argument*/NULL);
		__this->set_lastGameScore_7(L_7);
		Scene_t2348375561  L_8 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_8;
		String_t* L_9 = Scene_get_name_m622963475((&V_2), /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_m3937257545(NULL /*static, unused*/, L_9, _stringLiteral537924729, /*hidden argument*/NULL);
		int32_t L_11 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, L_10, 0, /*hidden argument*/NULL);
		__this->set_bonusGameCount_8(L_11);
		int32_t L_12 = __this->get_highGameScore_5();
		__this->set_lastHighGameScore_6(L_12);
		return;
	}
}
// System.Void CRGTGameManager::UpdateGameData()
extern "C"  void CRGTGameManager_UpdateGameData_m3678386969 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_gameScore_13();
		int32_t L_1 = __this->get_highGameScore_5();
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_gameScore_13();
		__this->set_highGameScore_5(L_2);
	}

IL_001d:
	{
		bool L_3 = __this->get_isGameOver_11();
		if (L_3)
		{
			goto IL_0081;
		}
	}
	{
		Text_t1901882714 * L_4 = __this->get_gameScoreText_22();
		int32_t* L_5 = __this->get_address_of_gameScore_13();
		String_t* L_6 = Int32_ToString_m141394615(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_6);
		Text_t1901882714 * L_7 = __this->get_gameBestScoreText_23();
		int32_t* L_8 = __this->get_address_of_highGameScore_5();
		String_t* L_9 = Int32_ToString_m141394615(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_9);
		Text_t1901882714 * L_10 = __this->get_gameLastScoreText_24();
		int32_t* L_11 = __this->get_address_of_lastGameScore_7();
		String_t* L_12 = Int32_ToString_m141394615(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_12);
		goto IL_00b9;
	}

IL_0081:
	{
		Text_t1901882714 * L_13 = __this->get_gameOverScoreText_26();
		int32_t* L_14 = __this->get_address_of_lastGameScore_7();
		String_t* L_15 = Int32_ToString_m141394615(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, L_15);
		Text_t1901882714 * L_16 = __this->get_gameOverHighScoreText_28();
		int32_t* L_17 = __this->get_address_of_highGameScore_5();
		String_t* L_18 = Int32_ToString_m141394615(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, L_18);
	}

IL_00b9:
	{
		return;
	}
}
// System.Void CRGTGameManager::SaveGameData()
extern "C"  void CRGTGameManager_SaveGameData_m1501793627 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager_SaveGameData_m1501793627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t2348375561  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Scene_t2348375561  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m622963475((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, L_1, _stringLiteral16531513, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_highGameScore_5();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Scene_t2348375561  L_4 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = Scene_get_name_m622963475((&V_1), /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, L_5, _stringLiteral4048657555, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_lastGameScore_7();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Scene_t2348375561  L_8 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_8;
		String_t* L_9 = Scene_get_name_m622963475((&V_2), /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_m3937257545(NULL /*static, unused*/, L_9, _stringLiteral537924729, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_bonusGameCount_8();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::ShowGameMenu()
extern "C"  void CRGTGameManager_ShowGameMenu_m2205210472 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_gameOverCanvas_33();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_pauseCanvas_32();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_gameCanvas_31();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_menuCanvas_30();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		CRGTGameManager_DisplayBanner_m3605215792(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::ShowGamePlayMenu()
extern "C"  void CRGTGameManager_ShowGamePlayMenu_m4164765505 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_gameCanvas_31();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_gameOverCanvas_33();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_pauseCanvas_32();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_menuCanvas_30();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		CRGTGameManager_DisplayBanner_m3605215792(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::ShowPauseMenu()
extern "C"  void CRGTGameManager_ShowPauseMenu_m2882739331 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_gameCanvas_31();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_gameOverCanvas_33();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_pauseCanvas_32();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_menuCanvas_30();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		CRGTGameManager_DisplayBanner_m3605215792(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::ShowGameOverMenu()
extern "C"  void CRGTGameManager_ShowGameOverMenu_m1957943842 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_gameOverCanvas_33();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_pauseCanvas_32();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_gameCanvas_31();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_menuCanvas_30();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		CRGTGameManager_DisplayBanner_m3605215792(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameMenu()
extern "C"  void CRGTGameManager_GameMenu_m4003783681 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_startGameSpeed_3();
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		CRGTGameManager_ShowGameMenu_m2205210472(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameStart()
extern "C"  void CRGTGameManager_GameStart_m2936842800 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_isGamePaused_12();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		CRGTGameManager_GameResume_m2309066240(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		__this->set_isGameOver_11((bool)0);
		__this->set_gameScore_13(0);
		float L_1 = __this->get_startSpawnSpeed_14();
		__this->set_spawnSpeed_10(L_1);
		float L_2 = __this->get_spawnSpeed_10();
		__this->set_spawnTime_9(L_2);
		CRGTGameManager_RequestInterstial_m2120773455(__this, /*hidden argument*/NULL);
		CRGTGameManager_DisplayInterstial_m49774147(__this, /*hidden argument*/NULL);
		CRGTGameManager_CleanUpScene_m4197824408(__this, /*hidden argument*/NULL);
		CRGTGameManager_UpdateGameData_m3678386969(__this, /*hidden argument*/NULL);
		CRGTGameManager_ShowGamePlayMenu_m4164765505(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_startGameSpeed_3();
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameRestart()
extern "C"  void CRGTGameManager_GameRestart_m1084859009 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		CRGTGameManager_GameStart_m2936842800(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GamePause()
extern "C"  void CRGTGameManager_GamePause_m2341877934 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		__this->set_isGamePaused_12((bool)1);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		CRGTGameManager_ShowPauseMenu_m2882739331(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameResume()
extern "C"  void CRGTGameManager_GameResume_m2309066240 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		__this->set_isGamePaused_12((bool)0);
		float L_0 = __this->get_startGameSpeed_3();
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		CRGTGameManager_ShowGamePlayMenu_m4164765505(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameStop()
extern "C"  void CRGTGameManager_GameStop_m607461065 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		CRGTGameManager_GameOver_m3937302641(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameOver()
extern "C"  void CRGTGameManager_GameOver_m3937302641 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager_GameOver_m3937302641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_isGameOver_11((bool)1);
		CRGTGameManager_CleanUpScene_m4197824408(__this, /*hidden argument*/NULL);
		CRGTGameManager_DisplayInterstial_m49774147(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_gameScore_13();
		__this->set_lastGameScore_7(L_0);
		CRGTGameManager_UpdateGameData_m3678386969(__this, /*hidden argument*/NULL);
		CRGTGameManager_SaveGameData_m1501793627(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_gameScore_13();
		int32_t L_2 = __this->get_lastHighGameScore_6();
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_005d;
		}
	}
	{
		Text_t1901882714 * L_3 = __this->get_gameOverNewText_27();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, _stringLiteral727109017);
		int32_t L_4 = __this->get_gameScore_13();
		__this->set_lastHighGameScore_6(L_4);
		goto IL_006d;
	}

IL_005d:
	{
		Text_t1901882714 * L_5 = __this->get_gameOverNewText_27();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
	}

IL_006d:
	{
		__this->set_gameScore_13(0);
		CRGTGameManager_ShowGameOverMenu_m1957943842(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameQuit()
extern "C"  void CRGTGameManager_GameQuit_m1354327122 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		CRGTGameManager_SaveGameData_m1501793627(__this, /*hidden argument*/NULL);
		CRGTGameManager_DisplayInterstial_m49774147(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::GameQuitNow()
extern "C"  void CRGTGameManager_GameQuitNow_m3158605986 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_ButtonSound_m513884379(__this, /*hidden argument*/NULL);
		Application_Quit_m470877999(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::UpdateScore(System.Int32)
extern "C"  void CRGTGameManager_UpdateScore_m1298736932 (CRGTGameManager_t3284010685 * __this, int32_t ___scoreValue0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_gameScore_13();
		int32_t L_1 = ___scoreValue0;
		__this->set_gameScore_13(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		CRGTGameManager_UpdateGameData_m3678386969(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::ButtonSound()
extern "C"  void CRGTGameManager_ButtonSound_m513884379 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTGameManager_ButtonSound_m513884379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioClip_t3680889665 * L_0 = __this->get_buttonClick_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var);
		CRGTSoundManager_t434062966 * L_2 = ((CRGTSoundManager_t434062966_StaticFields*)il2cpp_codegen_static_fields_for(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var))->get_instance_2();
		AudioClip_t3680889665 * L_3 = __this->get_buttonClick_21();
		NullCheck(L_2);
		CRGTSoundManager_PlaySound_m636038493(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void CRGTGameManager::InterstialDone()
extern "C"  void CRGTGameManager_InterstialDone_m2107184598 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		CRGTGameManager_GameQuitNow_m3158605986(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTGameManager::RequestInterstial()
extern "C"  void CRGTGameManager_RequestInterstial_m2120773455 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CRGTGameManager::DisplayInterstial()
extern "C"  void CRGTGameManager_DisplayInterstial_m49774147 (CRGTGameManager_t3284010685 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CRGTGameManager::DisplayBanner(System.Boolean)
extern "C"  void CRGTGameManager_DisplayBanner_m3605215792 (CRGTGameManager_t3284010685 * __this, bool ___show0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CRGTGameManager::.cctor()
extern "C"  void CRGTGameManager__cctor_m4107791513 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTMaterialMover::.ctor()
extern "C"  void CRGTMaterialMover__ctor_m1522289060 (CRGTMaterialMover_t3899902404 * __this, const RuntimeMethod* method)
{
	{
		__this->set_materialSpeedY_3((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTMaterialMover::Start()
extern "C"  void CRGTMaterialMover_Start_m3587889192 (CRGTMaterialMover_t3899902404 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTMaterialMover_Start_m3587889192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		__this->set_materialRenderer_5(L_0);
		return;
	}
}
// System.Void CRGTMaterialMover::Update()
extern "C"  void CRGTMaterialMover_Update_m311836227 (CRGTMaterialMover_t3899902404 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTMaterialMover_Update_m311836227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_materialSpeedX_2();
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_1 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_1);
		float L_2 = L_1->get_gameSpeed_4();
		float L_3 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_materialSpeedY_3();
		CRGTGameManager_t3284010685 * L_5 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_5);
		float L_6 = L_5->get_gameSpeed_4();
		float L_7 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3970636864((&L_8), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), (float)L_3)), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_6)), (float)L_7)), /*hidden argument*/NULL);
		__this->set_materialOffset_4(L_8);
		Renderer_t2627027031 * L_9 = __this->get_materialRenderer_5();
		NullCheck(L_9);
		Material_t340375123 * L_10 = Renderer_get_material_m4171603682(L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = __this->get_materialOffset_4();
		NullCheck(L_10);
		Material_set_mainTextureOffset_m2452486895(L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTObjectDestroyer::.ctor()
extern "C"  void CRGTObjectDestroyer__ctor_m303456916 (CRGTObjectDestroyer_t475906085 * __this, const RuntimeMethod* method)
{
	{
		__this->set_removeSelf_4((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTObjectDestroyer::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void CRGTObjectDestroyer_OnTriggerEnter2D_m3103412951 (CRGTObjectDestroyer_t475906085 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTObjectDestroyer_OnTriggerEnter2D_m3103412951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral1390680557, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004d;
		}
	}
	{
		bool L_3 = __this->get_removeTarget_2();
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		Collider2D_t2806799626 * L_4 = ___other0;
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_removeTargetTime_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_7 = __this->get_removeSelf_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_removeSelfTime_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTObjectMover::.ctor()
extern "C"  void CRGTObjectMover__ctor_m704622753 (CRGTObjectMover_t1733178804 * __this, const RuntimeMethod* method)
{
	{
		__this->set_objectSpeed_2((-3.0f));
		__this->set_objectMoveY_4((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTObjectMover::Update()
extern "C"  void CRGTObjectMover_Update_m2405874294 (CRGTObjectMover_t1733178804 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTObjectMover_Update_m2405874294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_objectMoveX_3();
		float L_2 = __this->get_objectMoveY_4();
		float L_3 = __this->get_objectMoveZ_5();
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), L_1, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_objectSpeed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_7 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_7);
		float L_8 = L_7->get_gameSpeed_4();
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Translate_m1810197270(L_0, L_11, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTPlayerController::.ctor()
extern "C"  void CRGTPlayerController__ctor_m2467363767 (CRGTPlayerController_t3312304059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTPlayerController__ctor_m2467363767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_playerSideSpeed_5((6.0f));
		__this->set_movePlayerRot_6((bool)1);
		__this->set_playerRot_7((5.0f));
		__this->set_playerRotSpeed_8((4.0f));
		SingleU5BU5D_t1444911251* L_0 = ((SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeFieldHandle_t1871169219  L_1 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255366____U24fieldU2DAC9373DEC6917FF369D56F004DADC890EAA43D80_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, L_1, /*hidden argument*/NULL);
		__this->set_spawnPlayerXPos_9(L_0);
		__this->set_playerMinX_10((-2.8f));
		__this->set_playerMaxX_11((2.8f));
		__this->set_rotationMultiplier_18((2.1f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTPlayerController::Start()
extern "C"  void CRGTPlayerController_Start_m3101612879 (CRGTPlayerController_t3312304059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTPlayerController_Start_m3101612879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTPlayerController_t3312304059_il2cpp_TypeInfo_var);
		((CRGTPlayerController_t3312304059_StaticFields*)il2cpp_codegen_static_fields_for(CRGTPlayerController_t3312304059_il2cpp_TypeInfo_var))->set_isAlive_3((bool)1);
		Rigidbody2D_t939494601 * L_0 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		__this->set_rigBody2D_22(L_0);
		Rigidbody2D_t939494601 * L_1 = __this->get_rigBody2D_22();
		NullCheck(L_1);
		Vector2_t2156229523  L_2 = Rigidbody2D_get_position_m2575647076(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_1();
		__this->set_playerPosY_20(L_3);
		__this->set_playerVelX_21((0.0f));
		Rigidbody2D_t939494601 * L_4 = __this->get_rigBody2D_22();
		SingleU5BU5D_t1444911251* L_5 = __this->get_spawnPlayerXPos_9();
		SingleU5BU5D_t1444911251* L_6 = __this->get_spawnPlayerXPos_9();
		NullCheck(L_6);
		int32_t L_7 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_8 = L_7;
		float L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		float L_10 = __this->get_playerPosY_20();
		Vector3_t3722313464  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m3353183577((&L_11), L_9, L_10, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_12 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_4);
		Rigidbody2D_set_position_m1659195711(L_4, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTPlayerController::FixedUpdate()
extern "C"  void CRGTPlayerController_FixedUpdate_m90202351 (CRGTPlayerController_t3312304059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTPlayerController_FixedUpdate_m90202351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t1921856868  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2156229523  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		int32_t L_0 = __this->get_playerControlType_2();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Input_get_acceleration_m2528400370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		__this->set_playerVelX_21(L_2);
		goto IL_008c;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0081;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_4 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = Touch_get_phase_m214549210((&V_1), /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_007c;
		}
	}
	{
		Vector2_t2156229523  L_6 = Touch_get_position_m3109777936((&V_1), /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = (&V_2)->get_x_0();
		int32_t L_8 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_7) < ((float)((float)((float)(((float)((float)L_8)))/(float)(2.0f)))))))
		{
			goto IL_0071;
		}
	}
	{
		__this->set_playerVelX_21((-1.0f));
		goto IL_007c;
	}

IL_0071:
	{
		__this->set_playerVelX_21((1.0f));
	}

IL_007c:
	{
		goto IL_008c;
	}

IL_0081:
	{
		__this->set_playerVelX_21((0.0f));
	}

IL_008c:
	{
		Rigidbody2D_t939494601 * L_9 = __this->get_rigBody2D_22();
		float L_10 = __this->get_playerVelX_21();
		Vector3_t3722313464  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m3353183577((&L_11), L_10, (0.0f), (0.0f), /*hidden argument*/NULL);
		float L_12 = __this->get_playerSideSpeed_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_14 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rigidbody2D_set_velocity_m2898400508(L_9, L_14, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_15 = __this->get_rigBody2D_22();
		Rigidbody2D_t939494601 * L_16 = __this->get_rigBody2D_22();
		NullCheck(L_16);
		Vector2_t2156229523  L_17 = Rigidbody2D_get_position_m2575647076(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = (&V_3)->get_x_0();
		float L_19 = __this->get_playerMinX_10();
		float L_20 = __this->get_playerMaxX_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		float L_22 = __this->get_playerPosY_20();
		Vector3_t3722313464  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m3353183577((&L_23), L_21, L_22, (0.0f), /*hidden argument*/NULL);
		Vector2_t2156229523  L_24 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_15);
		Rigidbody2D_set_position_m1659195711(L_15, L_24, /*hidden argument*/NULL);
		bool L_25 = __this->get_movePlayerRot_6();
		if (!L_25)
		{
			goto IL_0197;
		}
	}
	{
		V_4 = (0.0f);
		Rigidbody2D_t939494601 * L_26 = __this->get_rigBody2D_22();
		NullCheck(L_26);
		Vector2_t2156229523  L_27 = Rigidbody2D_get_position_m2575647076(L_26, /*hidden argument*/NULL);
		V_5 = L_27;
		float L_28 = (&V_5)->get_x_0();
		float L_29 = __this->get_playerMaxX_11();
		if ((!(((float)L_28) < ((float)L_29))))
		{
			goto IL_0166;
		}
	}
	{
		Rigidbody2D_t939494601 * L_30 = __this->get_rigBody2D_22();
		NullCheck(L_30);
		Vector2_t2156229523  L_31 = Rigidbody2D_get_position_m2575647076(L_30, /*hidden argument*/NULL);
		V_6 = L_31;
		float L_32 = (&V_6)->get_x_0();
		float L_33 = __this->get_playerMinX_10();
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_0166;
		}
	}
	{
		Rigidbody2D_t939494601 * L_34 = __this->get_rigBody2D_22();
		NullCheck(L_34);
		Vector2_t2156229523  L_35 = Rigidbody2D_get_velocity_m366589732(L_34, /*hidden argument*/NULL);
		V_7 = L_35;
		float L_36 = (&V_7)->get_x_0();
		V_4 = L_36;
	}

IL_0166:
	{
		Rigidbody2D_t939494601 * L_37 = __this->get_rigBody2D_22();
		Rigidbody2D_t939494601 * L_38 = __this->get_rigBody2D_22();
		NullCheck(L_38);
		float L_39 = Rigidbody2D_get_rotation_m1584227507(L_38, /*hidden argument*/NULL);
		float L_40 = V_4;
		float L_41 = __this->get_playerRot_7();
		float L_42 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_43 = __this->get_playerRotSpeed_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_44 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_39, ((float)il2cpp_codegen_multiply((float)L_40, (float)((-L_41)))), ((float)il2cpp_codegen_multiply((float)L_42, (float)L_43)), /*hidden argument*/NULL);
		NullCheck(L_37);
		Rigidbody2D_set_rotation_m1493600238(L_37, L_44, /*hidden argument*/NULL);
	}

IL_0197:
	{
		return;
	}
}
// System.Void CRGTPlayerController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void CRGTPlayerController_OnTriggerEnter2D_m1499976993 (CRGTPlayerController_t3312304059 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTPlayerController_OnTriggerEnter2D_m1499976993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral1390680557, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		AudioClip_t3680889665 * L_3 = __this->get_carCrash_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var);
		CRGTSoundManager_t434062966 * L_5 = ((CRGTSoundManager_t434062966_StaticFields*)il2cpp_codegen_static_fields_for(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var))->get_instance_2();
		AudioClip_t3680889665 * L_6 = __this->get_carCrash_19();
		NullCheck(L_5);
		CRGTSoundManager_PlaySound_m636038493(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_7 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_7);
		CRGTGameManager_GameOver_m3937302641(L_7, /*hidden argument*/NULL);
	}

IL_003f:
	{
		Collider2D_t2806799626 * L_8 = ___other0;
		NullCheck(L_8);
		String_t* L_9 = Component_get_tag_m2716693327(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral1581558434, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_11 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_11);
		bool L_12 = L_11->get_isGameOver_11();
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_13 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_13);
		bool L_14 = L_13->get_isGamePaused_12();
		if (!L_14)
		{
			goto IL_008c;
		}
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var);
		CRGTSoundManager_t434062966 * L_15 = ((CRGTSoundManager_t434062966_StaticFields*)il2cpp_codegen_static_fields_for(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var))->get_instance_2();
		AudioClip_t3680889665 * L_16 = __this->get_carCrash_19();
		NullCheck(L_15);
		CRGTSoundManager_PlaySound_m636038493(L_15, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var);
		CRGTGameManager_t3284010685 * L_17 = ((CRGTGameManager_t3284010685_StaticFields*)il2cpp_codegen_static_fields_for(CRGTGameManager_t3284010685_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_17);
		CRGTGameManager_GameOver_m3937302641(L_17, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void CRGTPlayerController::.cctor()
extern "C"  void CRGTPlayerController__cctor_m1370838780 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTPlayerController__cctor_m1370838780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((CRGTPlayerController_t3312304059_StaticFields*)il2cpp_codegen_static_fields_for(CRGTPlayerController_t3312304059_il2cpp_TypeInfo_var))->set_playerSpeed_4((1.0f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTRemoveAfterTime::.ctor()
extern "C"  void CRGTRemoveAfterTime__ctor_m2753249328 (CRGTRemoveAfterTime_t3231471983 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTRemoveAfterTime::Start()
extern "C"  void CRGTRemoveAfterTime_Start_m163819186 (CRGTRemoveAfterTime_t3231471983 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTRemoveAfterTime_Start_m163819186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_removeAfterTime_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTRemoveOnTouch::.ctor()
extern "C"  void CRGTRemoveOnTouch__ctor_m746880976 (CRGTRemoveOnTouch_t2211313123 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTRemoveOnTouch__ctor_m746880976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_TargetTag_2(_stringLiteral3076559204);
		__this->set_removeSelf_5((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTRemoveOnTouch::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void CRGTRemoveOnTouch_OnTriggerEnter2D_m480148688 (CRGTRemoveOnTouch_t2211313123 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTRemoveOnTouch_OnTriggerEnter2D_m480148688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_TargetTag_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004e;
		}
	}
	{
		bool L_4 = __this->get_removeTarget_3();
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		Collider2D_t2806799626 * L_5 = ___other0;
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_removeTargetTime_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0032:
	{
		bool L_8 = __this->get_removeSelf_5();
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		float L_10 = __this->get_removeSelfTime_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTSoundManager::.ctor()
extern "C"  void CRGTSoundManager__ctor_m1991707563 (CRGTSoundManager_t434062966 * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentState_6((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTSoundManager::Awake()
extern "C"  void CRGTSoundManager_Awake_m2042567216 (CRGTSoundManager_t434062966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTSoundManager_Awake_m2042567216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var);
		CRGTSoundManager_t434062966 * L_0 = ((CRGTSoundManager_t434062966_StaticFields*)il2cpp_codegen_static_fields_for(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var);
		((CRGTSoundManager_t434062966_StaticFields*)il2cpp_codegen_static_fields_for(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var))->set_instance_2(__this);
		goto IL_0036;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var);
		CRGTSoundManager_t434062966 * L_2 = ((CRGTSoundManager_t434062966_StaticFields*)il2cpp_codegen_static_fields_for(CRGTSoundManager_t434062966_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTSoundManager::Start()
extern "C"  void CRGTSoundManager_Start_m1243965269 (CRGTSoundManager_t434062966 * __this, const RuntimeMethod* method)
{
	{
		CRGTSoundManager_UpdateVolume_m617425764(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTSoundManager::UpdateVolume()
extern "C"  void CRGTSoundManager_UpdateVolume_m617425764 (CRGTSoundManager_t434062966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTSoundManager_UpdateVolume_m617425764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t3935305588 * L_0 = __this->get_efxSource_3();
		float L_1 = PlayerPrefs_GetFloat_m3293813615(NULL /*static, unused*/, _stringLiteral1691373009, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_set_volume_m1273312851(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_2 = __this->get_musicSource_4();
		float L_3 = PlayerPrefs_GetFloat_m3293813615(NULL /*static, unused*/, _stringLiteral534994172, (0.4f), /*hidden argument*/NULL);
		NullCheck(L_2);
		AudioSource_set_volume_m1273312851(L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral353759352, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		bool L_5 = Convert_ToBoolean_m2833489984(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_mute_7(L_5);
		bool L_6 = __this->get_mute_7();
		CRGTSoundManager_SetSoundOnOff_m376341164(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTSoundManager::PlaySound(UnityEngine.AudioClip)
extern "C"  void CRGTSoundManager_PlaySound_m636038493 (CRGTSoundManager_t434062966 * __this, AudioClip_t3680889665 * ___clip0, const RuntimeMethod* method)
{
	{
		AudioSource_t3935305588 * L_0 = __this->get_efxSource_3();
		AudioClip_t3680889665 * L_1 = ___clip0;
		NullCheck(L_0);
		AudioSource_set_clip_m31653938(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_2 = __this->get_efxSource_3();
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTSoundManager::ToggleSoundOnOff()
extern "C"  void CRGTSoundManager_ToggleSoundOnOff_m3930416408 (CRGTSoundManager_t434062966 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mute_7();
		CRGTSoundManager_SetSoundOnOff_m376341164(__this, (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTSoundManager::SetSoundOnOff(System.Boolean)
extern "C"  void CRGTSoundManager_SetSoundOnOff_m376341164 (CRGTSoundManager_t434062966 * __this, bool ___off0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTSoundManager_SetSoundOnOff_m376341164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CRGTSpriteToggle_t4126742132 * V_0 = NULL;
	CRGTSpriteToggleU5BU5D_t2845023549* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	{
		bool L_0 = ___off0;
		__this->set_mute_7(L_0);
		bool L_1 = __this->get_mute_7();
		G_B1_0 = _stringLiteral353759352;
		if (!L_1)
		{
			G_B2_0 = _stringLiteral353759352;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_001e:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_2 = __this->get_efxSource_3();
		bool L_3 = ___off0;
		NullCheck(L_2);
		AudioSource_set_mute_m3553377094(L_2, L_3, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_4 = __this->get_musicSource_4();
		bool L_5 = ___off0;
		NullCheck(L_4);
		AudioSource_set_mute_m3553377094(L_4, L_5, /*hidden argument*/NULL);
		CRGTSpriteToggleU5BU5D_t2845023549* L_6 = __this->get_Buttons_5();
		V_1 = L_6;
		V_2 = 0;
		goto IL_0065;
	}

IL_0049:
	{
		CRGTSpriteToggleU5BU5D_t2845023549* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		CRGTSpriteToggle_t4126742132 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		CRGTSpriteToggle_t4126742132 * L_11 = V_0;
		bool L_12 = __this->get_mute_7();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int32_t L_13 = Convert_ToInt32_m2100527582(NULL /*static, unused*/, (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		NullCheck(L_11);
		CRGTSpriteToggle_SetToggleValue_m212693942(L_11, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0065:
	{
		int32_t L_15 = V_2;
		CRGTSpriteToggleU5BU5D_t2845023549* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))))))
		{
			goto IL_0049;
		}
	}
	{
		return;
	}
}
// System.Boolean CRGTSoundManager::GetSoundEfxMute()
extern "C"  bool CRGTSoundManager_GetSoundEfxMute_m1012274421 (CRGTSoundManager_t434062966 * __this, const RuntimeMethod* method)
{
	{
		AudioSource_t3935305588 * L_0 = __this->get_efxSource_3();
		NullCheck(L_0);
		bool L_1 = AudioSource_get_mute_m2424389661(L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void CRGTSoundManager::.cctor()
extern "C"  void CRGTSoundManager__cctor_m1169778268 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CRGTSpriteToggle::.ctor()
extern "C"  void CRGTSpriteToggle__ctor_m2706052667 (CRGTSpriteToggle_t4126742132 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CRGTSpriteToggle::Start()
extern "C"  void CRGTSpriteToggle_Start_m2686693449 (CRGTSpriteToggle_t4126742132 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTSpriteToggle_Start_m2686693449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2670269651 * L_0 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_targetImage_5(L_0);
		return;
	}
}
// System.Void CRGTSpriteToggle::SetToggleValue(System.Int32)
extern "C"  void CRGTSpriteToggle_SetToggleValue_m212693942 (CRGTSpriteToggle_t4126742132 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CRGTSpriteToggle_SetToggleValue_m212693942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2670269651 * L_0 = __this->get_targetImage_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Image_t2670269651 * L_2 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_targetImage_5(L_2);
	}

IL_001d:
	{
		int32_t L_3 = ___value0;
		__this->set_toggleState_6(L_3);
		int32_t L_4 = __this->get_toggleState_6();
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0041;
		}
	}
	{
		Image_t2670269651 * L_5 = __this->get_targetImage_5();
		Sprite_t280657092 * L_6 = __this->get_ImageLocked_4();
		NullCheck(L_5);
		Image_set_sprite_m2369174689(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0041:
	{
		int32_t L_7 = __this->get_toggleState_6();
		if (L_7)
		{
			goto IL_005d;
		}
	}
	{
		Image_t2670269651 * L_8 = __this->get_targetImage_5();
		Sprite_t280657092 * L_9 = __this->get_ImageOff_3();
		NullCheck(L_8);
		Image_set_sprite_m2369174689(L_8, L_9, /*hidden argument*/NULL);
	}

IL_005d:
	{
		int32_t L_10 = __this->get_toggleState_6();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		Image_t2670269651 * L_11 = __this->get_targetImage_5();
		Sprite_t280657092 * L_12 = __this->get_ImageOn_2();
		NullCheck(L_11);
		Image_set_sprite_m2369174689(L_11, L_12, /*hidden argument*/NULL);
	}

IL_007a:
	{
		return;
	}
}
// System.Int32 CRGTSpriteToggle::get_ToggleState()
extern "C"  int32_t CRGTSpriteToggle_get_ToggleState_m1399189587 (CRGTSpriteToggle_t4126742132 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_toggleState_6();
		return L_0;
	}
}
// System.Void CRGTSpriteToggle::set_ToggleState(System.Int32)
extern "C"  void CRGTSpriteToggle_set_ToggleState_m3510598547 (CRGTSpriteToggle_t4126742132 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		CRGTSpriteToggle_SetToggleValue_m212693942(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GB::.ctor()
extern "C"  void GB__ctor_m2884495921 (GB_t166534947 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GB::.cctor()
extern "C"  void GB__cctor_m595848670 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GB__cctor_m595848670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455301613);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LangCtrl::.ctor()
extern "C"  void LangCtrl__ctor_m61013621 (LangCtrl_t2672849805 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LangCtrl::Start()
extern "C"  void LangCtrl_Start_m1152149911 (LangCtrl_t2672849805 * __this, const RuntimeMethod* method)
{
	{
		LangCtrl_SetLang_m691616426(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LangCtrl::SetLang()
extern "C"  void LangCtrl_SetLang_m691616426 (LangCtrl_t2672849805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LangCtrl_SetLang_m691616426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TextAsset_t3022178571 * V_0 = NULL;
	String_t* V_1 = NULL;
	JsonData_t1524858407 * V_2 = NULL;
	{
		LangCtrl_CheckLangType_m87832360(__this, /*hidden argument*/NULL);
		String_t* L_0 = __this->get_m_ItemTxt_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		Text_t1901882714 * L_2 = Component_GetComponent_TisText_t1901882714_m4196288697(__this, /*hidden argument*/Component_GetComponent_TisText_t1901882714_m4196288697_RuntimeMethod_var);
		__this->set_m_Text_3(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->get_g_LangType_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral184291000, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->get_g_LangType_2();
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3244380236, L_5, /*hidden argument*/NULL);
		TextAsset_t3022178571 * L_7 = Resources_Load_TisTextAsset_t3022178571_m724759995(NULL /*static, unused*/, L_6, /*hidden argument*/Resources_Load_TisTextAsset_t3022178571_m724759995_RuntimeMethod_var);
		V_0 = L_7;
		TextAsset_t3022178571 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = TextAsset_get_text_m2027878391(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t3815285241_il2cpp_TypeInfo_var);
		JsonData_t1524858407 * L_11 = JsonMapper_ToObject_m190320911(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		JsonData_t1524858407 * L_12 = V_2;
		String_t* L_13 = __this->get_m_ItemTxt_2();
		NullCheck(L_12);
		JsonData_t1524858407 * L_14 = JsonData_get_Item_m1449041990(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Text_t1901882714 * L_16 = __this->get_m_Text_3();
		JsonData_t1524858407 * L_17 = V_2;
		String_t* L_18 = __this->get_m_ItemTxt_2();
		NullCheck(L_17);
		JsonData_t1524858407 * L_19 = JsonData_get_Item_m1449041990(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, L_20);
		String_t* L_21 = ((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->get_g_LangType_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral931865376, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LangCtrl::CheckLangType()
extern "C"  void LangCtrl_CheckLangType_m87832360 (LangCtrl_t2672849805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LangCtrl_CheckLangType_m87832360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = Application_get_systemLanguage_m3110370732(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)7)))
		{
			case 0:
			{
				goto IL_0087;
			}
			case 1:
			{
				goto IL_0096;
			}
			case 2:
			{
				goto IL_0177;
			}
			case 3:
			{
				goto IL_00a5;
			}
			case 4:
			{
				goto IL_0177;
			}
			case 5:
			{
				goto IL_0177;
			}
			case 6:
			{
				goto IL_00b4;
			}
			case 7:
			{
				goto IL_00c3;
			}
			case 8:
			{
				goto IL_00d2;
			}
			case 9:
			{
				goto IL_00e1;
			}
			case 10:
			{
				goto IL_0177;
			}
			case 11:
			{
				goto IL_0177;
			}
			case 12:
			{
				goto IL_0177;
			}
			case 13:
			{
				goto IL_0177;
			}
			case 14:
			{
				goto IL_0177;
			}
			case 15:
			{
				goto IL_00f0;
			}
			case 16:
			{
				goto IL_00ff;
			}
			case 17:
			{
				goto IL_0177;
			}
			case 18:
			{
				goto IL_0177;
			}
			case 19:
			{
				goto IL_010e;
			}
			case 20:
			{
				goto IL_011d;
			}
			case 21:
			{
				goto IL_012c;
			}
			case 22:
			{
				goto IL_013b;
			}
			case 23:
			{
				goto IL_014a;
			}
			case 24:
			{
				goto IL_0177;
			}
			case 25:
			{
				goto IL_0177;
			}
			case 26:
			{
				goto IL_0177;
			}
			case 27:
			{
				goto IL_0159;
			}
			case 28:
			{
				goto IL_0168;
			}
		}
	}
	{
		goto IL_0177;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3456153565);
		goto IL_0186;
	}

IL_0096:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454646236);
		goto IL_0186;
	}

IL_00a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454842843);
		goto IL_0186;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455170522);
		goto IL_0186;
	}

IL_00c3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455629274);
		goto IL_0186;
	}

IL_00d2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454384089);
		goto IL_0186;
	}

IL_00e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455629273);
		goto IL_0186;
	}

IL_00f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455760358);
		goto IL_0186;
	}

IL_00ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454777317);
		goto IL_0186;
	}

IL_010e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454777314);
		goto IL_0186;
	}

IL_011d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454973936);
		goto IL_0186;
	}

IL_012c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454777328);
		goto IL_0186;
	}

IL_013b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454777326);
		goto IL_0186;
	}

IL_014a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455432686);
		goto IL_0186;
	}

IL_0159:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455760365);
		goto IL_0186;
	}

IL_0168:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3455301613);
		goto IL_0186;
	}

IL_0177:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GB_t166534947_il2cpp_TypeInfo_var);
		((GB_t166534947_StaticFields*)il2cpp_codegen_static_fields_for(GB_t166534947_il2cpp_TypeInfo_var))->set_g_LangType_2(_stringLiteral3454842843);
		goto IL_0186;
	}

IL_0186:
	{
		return;
	}
}
// System.Void LangCtrl::Update()
extern "C"  void LangCtrl_Update_m1346771639 (LangCtrl_t2672849805 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
